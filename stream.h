#define STRM_TYPE_MASK 255<<20
#define FILE_STRM 1<<20
#define READ 1<<16
#define WRITE 2<<16
#define SEEK 4<<16
#define EOFBIT 8<<16
#define EOFP(s) (STRM_FLAGS(s) & EOFBIT)
#define SET_STRM(s, t, f) ((s)->type|=(t)|(f))
#define STRM_FLAGS(s) ((s)->type)
#define STRM_TYPE(s) ((s)->type & (STRM_TYPE_MASK))
#define STRM_PTR(o) ((struct stream *)(o)->ptr.stream)
/* nil is a valid stream (std(in|out))... */
#define STREAMP(s) (TYPE(s)==STRM || NULLP(s))
#define WSTRMP(s) (NULLP(s) || STRM_FLAGS(s) & WRITE)
#define RSTRMP(s) (NULLP(s) || STRM_FLAGS(s) & READ)
/* ... but it's not seekable. */
#define RASTRMP(s) (STRM_FLAGS(s) & SEEK)

object *s_out;			/* stdout */
object *s_in;			/* stdin */

void writs(char *, object *);
void writc(int, object *);
int readc(object *);
void unreadc(int, object *);
void sclose(object *);
void streaminit();

#include "include/stream-lispsyms.h"
#include "include/stream-lispdecls.h"
