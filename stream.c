#include "tl.h"
#include "stream.h"

#include "exceptn.h"
#include "mem.h"
#include "subr.h"

#define STRM_FUNC(s) (STRM_TYPE(s)>>20)-1

object *current_out;		/* Current output stream. */
object *current_in;		/* Etc. */

struct stream_type {
     void (*write_char)(int, object *);
     int (*read_char)(object *);
     void (*unread_char)(int, object *);
     off_t (*tell)(object *);
     void (*seek)(object *, off_t, int);
     void (*close)(object *);
};

struct stream_type **stream_types;
int n_str_types;

void file_writec(int, object *);
int file_readc(object *);
void file_unreadc(int, object *);
void file_close(object *);

DEFUN("streamp", streamp, 1, 1)
  (object *s)
{
     return STREAMP(s) ? CVAR(t) : CVAR(nil);
}

DEFUN("read-stream-p", read_strm_p, 1, 1)
  (object *s)
{
     return STREAMP(s) && RSTRMP(s) ? CVAR(t) : CVAR(nil);
}

DEFUN("write-stream-p", write_strm_p, 1, 1)
  (object *s)
{
     return STREAMP(s) && WSTRMP(s) ? CVAR(t) : CVAR(nil);
}

DEFUN("current-output", get_curr_out, 0, 0)
  ()
{
     return current_out;
}

DEFUN("set-output", set_output, 1, 1)
  (object *s)
{
     ASSERT_TYPE(STREAMP(s), CVAR(streamp), s);
     ASSERT_TYPE(WSTRMP(s), CVAR(write_strm_p), s);
     return current_out=s;
}

DEFUN("current-input", get_curr_in, 0, 0)
  ()
{
     return current_in;
}

DEFUN("set-input", set_input, 1, 1)
  (object *s)
{
     ASSERT_TYPE(STREAMP(s), CVAR(streamp), s);
     ASSERT_TYPE(RSTRMP(s), CVAR(read_strm_p), s);
     return current_in=s;
}

void writc(int c, object *s)
{
     if (NULLP(s))
	  s=current_out;
     stream_types[STRM_FUNC(s)]->write_char(c, s);
}

void file_writec(int c, object *s)
{
     fputc(c, STRM_PTR(s)->type.file);
}

int readc(object *s)
{
     if (NULLP(s))
	  s=current_in;
     return stream_types[STRM_FUNC(s)]->read_char(s);
}

int file_readc(object *s)
{
     return fgetc(STRM_PTR(s)->type.file);
}

void unreadc(int c, object *s)
{
     if (NULLP(s))
	  s=current_in;
     stream_types[STRM_FUNC(s)]->unread_char(c, s);
}

void file_unreadc(int c, object *s)
{
     ungetc(c, STRM_PTR(s)->type.file);
}

void sclose(object *s)
{
     stream_types[STRM_FUNC(s)]->close(s);
}

void file_close(object *s)
{
     fclose(STRM_PTR(s)->type.file);
}

void writs(char *ing, object *eam)
{
     if (STRM_TYPE(eam)==FILE_STRM)
	  fputs(ing, STRM_PTR(eam)->type.file);
     else
	  while (*ing)
	       writc(*ing++, eam);
}

DEFUN("write-char", write_char, 1, 2)
  (object *c, object *s)
{
     ASSERT_TYPE(INTP(c), CVAR(intp), c);
     ASSERT_TYPE(STREAMP(s), CVAR(streamp), s);
     writc(INTVAL(c), s);
     return c;
}

DEFUN("read-char", read_ch, 0, 1)
  (object *s)
{
     ASSERT_TYPE(STREAMP(s), CVAR(streamp), s);
     return MKINT(readc(s));
}

DEFUN("unread-char", unread_char, 1, 2)
  (object *c, object *s)
{
     ASSERT_TYPE(STREAMP(s), CVAR(streamp), s);
     unreadc(INTVAL(c), s);
     return c;
}



struct stream_type file_spec =
{
     file_writec,
     file_readc,
     file_unreadc,
     NULL,
     NULL,
     file_close
};

#include "include/stream-lispinit.h"

void streaminit()
{
     n_str_types=1;
     stream_types=xmalloc(n_str_types*sizeof(struct stream_type *));
     stream_types[0]=&file_spec;

     current_in=s_in=new_object(STRM);
     current_out=s_out=new_object(STRM);
     STRM_PTR(s_in)->type.file=stdin;
     STRM_PTR(s_in)->name="stdin";
     STRM_PTR(s_out)->type.file=stdout;
     STRM_PTR(s_out)->name="stdout";
     SET_STRM(s_in, FILE_STRM, READ);
     SET_STRM(s_out, FILE_STRM, WRITE);
     stream_lispinit();
}
