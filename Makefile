CC = gcc

CDEBUG = -ggdb3 -DDEBUG
CEXTRA =

CFLAGS = -Wall $(CDEBUG) $(CEXTRA)

MODULE_SOURCES = eval.c print.c stream.c read.c mem.c types.c \
		subr.c exceptn.c

SOURCES = $(MODULE_SOURCES) main.c

MODULE_HEADERS = $(MODULE_SOURCES:.c=.h)

HEADERS = $(MODULE_HEADERS) tl.h

MODULE_OBJECTS = $(MODULE_SOURCES:.c=.o)

OBJECTS = $(MODULE_OBJECTS) main.o

DEPS = $(SOURCES:.c=.d)

tlt: $(OBJECTS)
	$(CC) -o $@ $(OBJECTS) $(CFLAGS)

-include $(DEPS)

%.o: %.c
	$(CC) $(CFLAGS) -c -MMD -o $@ $<

include/%-lispsyms.h : %.c scripts/make-lispsyms-h
	<$< CPP="$(CPP)" scripts/make-lispsyms-h >$@.new \
		&& mv $@.new $@

include/%-lispdecls.h: %.c scripts/make-lispdecls-h
	<$< CPP="$(CPP)" scripts/make-lispdecls-h >$@.new \
		&& mv $@.new $@

include/%-lispinit.h: %.c scripts/make-lispinit-h
	<$< CPP="$(CPP)" scripts/make-lispinit-h $* >$@.new \
		&& mv $@.new $@

.PHONY: clean
clean:
	rm -f $(OBJECTS) $(DEPS) tl tlt

tags: TAGS

TAGS: etags.regexes $(SOURCES) $(HEADERS) include/*.h
	etags --regex=@etags.regexes $(SOURCES) $(HEADERS) include/*.h
