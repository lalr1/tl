#include "tl.h"
#include "types.h"

#include "mem.h"
#include "print.h"
#include "stream.h"
#include "subr.h"

#define SBFULL(sb) ((sb)->nextfree - (sb)->block) >= BLOCKSIZE
#define CELLSIZE(sb) ((sb->nextfree)>>16)
#define SETCELLSIZE(sb, i) (sb)->nextfree|=((i)<<16)
#define SETNFREE(sb, i) (sb)->nextfree|=((i)&0xffff)
#define NFREE(sb) ((sb)->nextfree&0xffff)

struct sblock {
     struct sblock *next;
     struct symbol *nextfree;
     struct symbol block[0];
};

struct sblock *sblist_head;
struct sblock *sblist_current;
struct symbol **sfreelist;
int sflsize;
int sflsp=0;

int ttabsize;

object *sym_alloc();
void sym_mark(object *);
void sym_free(object *);
void sym_print(object *, object *);

object *subr_alloc();
void subr_free(object *);
void subr_print(object *, object *);

object *cons_alloc();
void cons_mark(object *);
void cons_print(object *, object *);
void printcdr(object *, object *);

object *arr_alloc();
void arr_mark(object *);
void arr_free(object *);
void arr_print(object *, object *);

object *stream_alloc();
void stream_free(object *);
void stream_print(object *, object *);

struct symbol *scell_alloc();
void scell_free(struct symbol *);
void free_sblocks();

void sym_print(object *o, object *s)
{
     if (!SYM_INTERNED_P(o))
	  writs("#:", s);
     writs(SYMNAME(o), s);
}

void subr_print(object *o, object *s)
{
     writs("#<subr ", s);
     writs(SUBRNAME(o), s);
     writc('>', s);
}

void cons_print(object *o, object *s)
{
     writc('(', s);
     prin1(CAR(o), s);
     printcdr(CDR(o), s);
}

void printcdr(object *o, object *s)
{
     if (NULLP(o))
	  writc(')', s);
     else if (!CONSP(o)) {
	  writs(" . ", s);
	  prin1(o, s);
	  writc(')', s);
     } else {
	  writc(' ', s);
	  prin1(CAR(o), s);
	  printcdr(CDR(o), s);
     }
}

object *sym_alloc()
{
     object *ret=objalloc();
     ret->ptr.sym=scell_alloc();
     return ret;
}

void sym_mark(object *a)
{
     gcmark(SYMVAL(a));
     gcmark(SYMFUNC(a));
     gcmark(SYMPLIST(a));
}

void sym_free(object *a)
{
     uninternatom(a);
     free(SYMNAME(a));
     scell_free(a->ptr.sym);
}

object *subr_alloc()
{
     object *ret=objalloc();
     ret->ptr.subr=xmalloc(sizeof(struct subr));
     return ret;
}

void subr_free(object *a)
{
     free((void **) a->ptr.subr);
}

object *cons_alloc()
{
     return (object *) ( ((ptrbits_t) objalloc()) | 4);
}

void cons_mark(object *a)
{
     gcmark((object *) CAR(a));
     gcmark((object *) CDR(a));
}     

object *arr_alloc()
{
     object *ret=objalloc();
     ret->ptr.array=xmalloc(sizeof(struct array));
     return ret;
}

void arr_mark(object *a)
{
     if (STRINGP(a))
	  return;
     int i;
     for (i=0; i<ARRAYLEN(a); i++)
	  gcmark(ARR_PTR(a)->type.vector[i]);
}

void arr_free(object *a)
{
     free(ARR_PTR(a)->type.string);
     free((void **) a->ptr.array);
}

void arr_print(object *a, object *s)
{
     int i;
     if (STRINGP(a)) {
	  if (!STRREF(a)) {
	       writs("\"\"", s);
	  } else {
	       writc('"', s);
	       writs(ARR_PTR(a)->type.string, s);
	       writc('"', s);
	  }
     } else {
	  writc('[', s);
	  for (i=0; i<ARRAYLEN(a); i++) {
	       prin1(ARR_PTR(a)->type.vector[i], s);
	       if (i+1!=ARRAYLEN(a))
		    writc(' ', s);
	       else
		    writc(']', s);
	  }
     }
}

object *make_string(char *s)
{
     object *ret=new_object(ARRAY);
     SET_STRING(ret);
     SET_ARRLEN(ret, strlen(s));
     STRREF(ret)=strdup(s);
     return ret;
}

object *array_to_vector(int len, object **arr)
{
     object *v=xmalloc(len);
     object *ret=new_object(ARRAY);
     SET_VECTOR(ret);
     SET_ARRLEN(ret, len/sizeof(object *));
     VECREF(ret)=memcpy(v, arr, len);
     return ret;
}

object *make_vector(int len, object *fill)
{
     int i;
     object **arr=alloca(len*sizeof(object *));
     for (i=0; i<len; i++)
	  arr[i]=fill;
     return array_to_vector(len*sizeof(object *), arr);
}

object *stream_alloc()
{
     object *ret=objalloc();
     ret->ptr.stream=xmalloc(sizeof(struct stream));
     return ret;
}

void stream_free(object *a)
{
     sclose(a);
     free(a->ptr.stream);
}

void stream_print(object *a, object *s)
{
     writs("#<stream \"", s);
     writs(STRM_PTR(a)->name, s);
     writs("\">", s);
}


struct type_spec stream_spec =
{
     "stream",
     stream_alloc,
     NULL,
     stream_print,
     NULL,
     stream_free
};

struct type_spec sym_spec = 
{
     "symbol",
     sym_alloc,
     sym_mark,
     sym_print,
     NULL,
     sym_free
};

struct type_spec subr_spec =
{
     "subr",
     subr_alloc,
     NULL,
     subr_print,
     NULL,
     subr_free
};

struct type_spec cons_spec =
{
     "cons",
     cons_alloc,
     cons_mark,
     cons_print,
     NULL,
     NULL
};

struct type_spec array_spec =
{
     "array",
     arr_alloc,
     arr_mark,
     arr_print,
     NULL,
     arr_free
};
     
struct type_spec def =
{
     "no type",
     NULL,
     NULL,
     NULL,
     NULL,
     NULL,
};

struct symbol *scell_alloc()
{
     struct sblock *s=sblist_current;
     if (sflsp)
	  return sfreelist[--sflsp];
     if (SBFULL(s)) {
	  sblist_current->next=xmalloc(sizeof(struct sblock)
				       +(BLOCKSIZE*sizeof(struct symbol)));
	  sblist_current=sblist_current->next;
	  sblist_current->nextfree=sblist_current->block;
     }
     return sblist_current->nextfree++;
}

void scell_free(struct symbol *sc)
{
     if (sflsp==sflsize)
	  sfreelist=xrealloc(sfreelist,
			     sizeof(struct symbol *)*(sflsize+=sflsize));
     memset(sc, 0, sizeof(struct symbol));
     sfreelist[sflsp++]=sc;
}

void free_sblocks()
{
     struct sblock *t, *s=sblist_head;
     while (s) {
	  t=s->next;
	  free(s);
	  s=t;
     }
}

#include "include/types-lispinit.h"

void typeinit()
{
     type_max=NTYPES;
     ttabsize=type_max+1;
     types=xmalloc(ttabsize*sizeof(struct type_spec *));
     types[0]=&def; 
     types[SUBR]=&subr_spec;
     types[SYMB]=&sym_spec;
     types[CONS]=&cons_spec;
     types[INTG]=0;
     types[STRM]=&stream_spec;
     types[ARRAY]=&array_spec;

     sfreelist=(struct symbol **) alloc_block(sizeof(struct symbol *));
     sflsize=BLOCKSIZE;
     sblist_head=sblist_current=xmalloc(sizeof(struct sblock)
				 + (BLOCKSIZE*sizeof(struct symbol)));
     sblist_head->nextfree=sblist_head->block;

     types_lispinit();
}

void typefinish()
{
     free_sblocks();
     free(types);
}

/* Register a new type. */
int register_type(struct type_spec *t)
{
     if (type_max+1==ttabsize)
	  types=xrealloc(types, (ttabsize*=2)*sizeof(struct type_spec *));
     types[type_max]=t;
     return type_max++;
}

