#define OSUBR(o) (o)->ptr.subr->subr
#define ARGSFIELD(o) (((o)->type)>>16)
#define SETARGS(s, n) (s)->type|=((n)<<16)
#define MINARGS(n) (n & 7)
#define MAXARGS(n) ((n & 0x3f)<<3)
#define NUMMINARGS(o) ARGSFIELD(o)&7
#define NUMMAXARGS(o) (ARGSFIELD(o)>>3)&0xf
#define ARGS_UNLIMITED 1<<4
#define ARGS_UNEVALLED 1<<5
#define UNLIMITED(o) ((ARGSFIELD(o)>>3) & (ARGS_UNLIMITED))
#define UNEVALLED(i) ((i)|ARGS_UNEVALLED)
#define SPECIAL(o) ((ARGSFIELD(o)>>3) & (ARGS_UNEVALLED))
#define SUBRNAME(o) (o)->ptr.subr->subrname

struct subrinit { /* info for addsubr() (in subr.c) */
     char *name; 		/* structs of this type are created by */
     object * (*func)();	/* the DEFUN() macro */
     unsigned int args;
};

object *addsubr(struct subrinit);

object *make_list(int, ...);

unsigned long xlength(object *);

object *make_subr(object * (*)(), int, int, char *);
void initsubrs();

#include "include/subr-lispsyms.h"
#include "include/subr-lispdecls.h"
