#define GCSAFE(o) ((object *)((ptrbits_t)o&~GCBIT)) /* mask out gc bit */
#define GC_CONSP(o) ((((ptrbits_t) CAR(o)) & 5) != 1)

/* Top of C stack, for gc stack-walker. Set by tlinit(). It's a void
 * ** (rather than a void *) so we can compare it to the current
 * pointer into the stack in gcstackwalk() without the compiler
 * worrying. */
void **gc_top_of_stack;

object *objalloc(void);
object *new_object(int);
void freeall(void);
object *intern(char *);
void gcmark(object *);

void *xmalloc(size_t);
void *xrealloc(void *, size_t);
object *alloc_block(int );
void meminit();
void meminit2();

#include "include/mem-lispsyms.h"
#include "include/mem-lispdecls.h"
