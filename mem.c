#include "tl.h"
#include "mem.h"

#include "eval.h"
#include "exceptn.h"
#include "read.h"
#include "subr.h"
#include "stream.h"
#include "types.h"

#define GET_FRAME __builtin_frame_address(0)

/* Information about entries in the table of 8k heap blocks. */
struct blocklist_entry {
     struct blocklist_entry *next;
     object *nextfree;
     object block[0];
};

int gensym_count=0;
int lastblock=0;
int objects_in_use=0;
object *freelist=NULL;
int freesp=0;

/* Global C variables that the garbage collector should
 * preserve. Initialised in meminit(). We make it an array of pointers
 * to pointers so that we needn't update it every time a protected
 * variable changes --- we just take what each element points to
 * (which is the address in static storage of the variable) and mark
 * what that points to (which is the variable's value).
 */
object **protected_vars[PROT_VARS+1];

/* The obarray, etc. This could be made more efficient by having each
 * bucket be either an `object *' or a `struct objlist *', so that
 * we'd incur no overhead on single objects. We could distinguish
 * between the two by setting a bit in the address --- perhaps the
 * cons bit, since the buckets will only ever hold symbols or lists of
 * symbols. */
object *obarray;
int obarraysize;

/* Table of 8k heap blocks */
struct blocklist_entry *blocklist_top, *blocklist_current;
int blocktabsize;

int freelistsize;

void freeobj(object *);
object *find_sym(char *);
object *make_sym(char *);
void gcsillywalk();
object *gc(void);
void initobarray(int);
int hash(char *);
object *put_sym(object *);
object *lisp_make_sym(object *);
object *lisp_intern(object *);
object *gensym(object *);
int is_in_heap(object *);

DEFVAR("nil", CVAR(nil), CVAR(nil));
DEFVAR("t", CVAR(t), CVAR(t));
DEFSYM("&optional", CVAR(Ampopt));
DEFSYM("&rest", CVAR(Amprest));
/* The number of object headers allocated before objalloc() calls
 * gc(). It's a Lisp variable so that it can be altered from Lisp, but
 * gc() will set it to 1024 if it's any less. */
DEFVAR("max-objects", CVAR(max_objects), MKINT(16384));


void *xmalloc(size_t size)
{
     void *ret=malloc(size);
     if (ret)
	  return memset(ret, 0, size);
     puts("malloc failed");
     exit(1);
}

void *xrealloc(void *p, size_t size)
{
     p=realloc(p, size);
     if (p)
	  return p;
     puts("realloc failed");
     exit(1);
}

/* Allocate a block of 1024*size bytes, and zero it. */
object *alloc_block(int size)
{
     int i=BLOCKSIZE*size;
     object *ret=xmalloc(i);
     memset(ret, 0, i);
     return ret;
}

/* Return the address of a fresh object */
object *new_object(int type)
{
     object *ret;
     object * (*f)()=types[type]->alloc;
     if (f)
	  ret=f();
     else
	  return 0;
     SETTYPE(ret, type);
     return ret;
}

/* Allocate an 8-byte chunk from an 8k heap block. Return an `object *' --
 * this can be used as a cons, either, since conses are also 8 bytes
 * in size. If the free list is not empty, return the first address on it.
 * If it is empty, and if all blocks are full, malloc() a new one.
 * The object * returned serves as a `header' for large objects or as the
 * body of a cons.
 * Conses are distinguished from other objects by the fact that bit 1 of
 * their address is artificially set by cons_alloc() in types.c. This works
 * because malloc() is guaranteed to return an 8-byte-aligned address.
 */
object *objalloc()
{
     objects_in_use++;
     if (freelist) {
	  object *r=freelist;
	  CAR(r)=0;		/* Get rid of free-marker. */
	  freelist=CDR(freelist);
	  freesp--;
	  return r;
     }
     if (CVAR(max_objects))
	  if (objects_in_use > INTVAL(SYMVAL(CVAR(max_objects))))
	       gc();
     struct blocklist_entry *b=blocklist_current;
     if (b->nextfree - b->block >= BLOCKSIZE) {
	  blocklist_current->next=xmalloc(sizeof(struct blocklist_entry)
					  +(BLOCKSIZE*sizeof(object)));
	  blocklist_current=blocklist_current->next;
	  blocklist_current->nextfree=blocklist_current->block;
     }
     return blocklist_current->nextfree++;
}

/* Check if an address lies within an allocated heap block.  Return
 * the index of the block in the block table + 1. The exact return
 * value isn't used yet, but could be useful in gathering heap
 * statistics. */
int is_in_heap(object *p)
{
     if (((ptrbits_t) p & 3)) /* Not aligned properly. */
	  return 0;
     int i=0;
     struct blocklist_entry *b=blocklist_top;
     for ( ; b; b=b->next, i++)
	  if (p>=b->block && p<b->nextfree)
	       return i+1;
     return 0;
}

/* Print some statistics regarding the heap blocks */
DEFUN("heap-info", heap_info, 0, 0)
  ()
{
     int i=0;
     struct blocklist_entry *b=blocklist_top;
     printf(" %d objects in use\n", objects_in_use);
     for ( ; b; b=b->next, i++) {
	  int x = (b->nextfree-b->block)*100/BLOCKSIZE;
	  printf(" block %d at 0x%lx", i, (ptrbits_t) b->block);
	  if (x<100)
	       printf(" %d%% used\n", x);
	  else
	       printf("\n");
     }
     printf(" %d objects on free list\n", freesp);
     return CVAR(t);
}

void freeobj(object *p)
{
     if (IMMEDP(p))
	  return;
     int i=GC_CONSP(p) ? CONS : TYPE(p);
     if (i>type_max)
	  return;		/* Something odd has happened. Run away! */
     objects_in_use--;
     void (*f)()=types[i]->free;
     if (f)
	  f(p);
     SET_FREE_CELL(p);
     CDR(p)=freelist;
     freelist=p;
     freesp++;
}

/* Clean up. See tl.h for the MPROBE macro. */
void freeall()
{
     object *l;
     int i;
     for (i=0; i<obarraysize; i++)
	  for (l=VECREF(obarray)[i]; l; l=CDR(l))
	       free(SYMNAME(CAR(l)));

     typefinish();
     sigfinish();
     readfinish();
     evalfinish();

     i=0;
     struct blocklist_entry *b=blocklist_top, *t;
     while (b) {
	  t=b->next;
	  free(b);
	  b=t;
     }
}

/* Initialise the obarray */
void initobarray(int sz)
{
/*      obarray=xmalloc(sz*sizeof(struct objlist *)); */
/*      memset(obarray, 0, sz*sizeof(struct objlist *)); */
     obarraysize=sz;

     obarray=make_vector(sz, NULL);
}

/* Bad hash function */
int hash(char *n)
{
     register unsigned int i=0,j=1,t=0;
     while (n[i])
	  t+=((int) n[i++]) * (j+=t); /* random, inefficient */
     return t%obarraysize;
}

/* Add a symbol to the obarray */
object *put_sym(object *o)
{
     int i=hash(SYMNAME(o));
     VECREF(obarray)[i]=cons(o, VECREF(obarray)[i]);
     SET_INTERNED(o);
     return o;
}	  

/* Create a new symbol */
object *make_sym(char *n)
{
     object *o=new_object(SYMB);
     SYMNAME(o)=(char *) strdup(n);
     SYMFUNC(o)=(object *) 0;
     if (n[0]==':') {       /* it's a keyword */
	  SYMVAL(o)=o;   /*  -- evaluates to itself */
	  SET_CONST(o);
     } else
	  SYMVAL(o)=(object *) 0;
     SYMPLIST(o)=CVAR(nil);
     return o;
}

DEFUN("gensym", gensym, 0, 1)
  (object *pref)
{
     char *cpref="G";
     if (!NULLP(pref)) {
	  ASSERT_TYPE(STRINGP(pref), CVAR(stringp), pref);
	  cpref=STRREF(pref);
     }
     int i=gensym_count, j=1 + (NULLP(pref) ? 1 : ARRAYLEN(pref));
     /* Cheap 'n' cheesy way to find 1 + floor(log_10(i)); i.e. number
      * of characters we need to print it. */
     for ( ; i>10; i/=10, j++)
	  ;
     char *g=alloca(j+2);
     snprintf(g, j+2, "%s%d", cpref, gensym_count++);
     return make_sym(g);
}

DEFUN("make-symbol", lisp_make_sym, 1, 1)
  (object *s)
{
     ASSERT_TYPE(STRINGP(s), CVAR(stringp), s);
     return make_sym(ARR_PTR(s)->type.string);
}

/* Check if a symbol is already interned */
object *find_sym(char *n)
{
     object *l=VECREF(obarray)[hash(n)];
     for ( ; l; l=CDR(l))
	  if (!strcmp(n, SYMNAME(CAR(l))))
	       return CAR(l);
     return (object *) 0;
}

/* Intern a symbol. */
object *intern(char *n)
{
     object *ret=find_sym(n);
     if (!ret)
	  return put_sym(make_sym(n));
     else
	  return ret;
}

DEFUN("intern", lisp_intern, 1, 1)
  (object *s)
{
     ASSERT_TYPE(STRINGP(s), CVAR(stringp), s);
     return intern(ARR_PTR(s)->type.string);
}

/* Remove a symbol from the obarray */
DEFUN("unintern", uninternatom, 1, 1)
  (object *p)
{
     ASSERT_TYPE(SYMBOLP(p), CVAR(symbolp), p);
     int i=hash(SYMNAME(p));
     object *l=VECREF(obarray)[i], *t;
     if (l) {
	  if (CAR(l)==p) {
	       VECREF(obarray)[i]=CDR(l);
	       SET_N_INTERNED(p);
	       return CVAR(t);
	  } else
	       l=CDR(l);
     }
     for (t=VECREF(obarray)[i]; l; l=CDR(l))
	  if (CAR(l)==p) {
	       CDR(t)=CDR(l);
	       SET_N_INTERNED(p);
	       return CVAR(t);
	  } else
	       t=l;

     return CVAR(nil);
}

/* Mark an object for preservation. */
void gcmark(object *a)
{
     object *real_addr;
     /* We might have been passed the car of a GC-marked cons. */
     a=GCSAFE(a);
     /* Check for void pointers here.  */
     if (!a || IMMEDP(a) || GCMARKED(a))
	  return;
     real_addr = CONSP(a) ? (object *)CONS_PTR(a) : a;
     GCMARK(real_addr);
     if (TYPE(a) > type_max)	/* We've been passed a bogus pointer --- */
	  return;		/* leave it alone. */
     void (*f)()=types[TYPE(a)]->mark;
     if (f)
	  f(a);
}

/* Walk the obarray and other important structures, marking
 * everything. We protect all the global variables in protected_vars,
 * all active read-macros and each frame on the eval-stack.
 * Catch-point handlers should be safe, since they'll be on either the
 * C stack or the eval-stack, if not both.  */
void gcsillywalk()
{
     register int i;
     for (i=0; i<obarraysize; i++) {
	  object *l=VECREF(obarray)[i];
	  for ( ; l; l=CDR(l))
	       gcmark(CAR(l));
     }
     /* Mark protected (i.e. global) C variables. */
     for (i=0; protected_vars[i]; i++)
	  gcmark(*(protected_vars[i]));
     /* Mark all read-macros. */
     for (i=0; i<=UCHAR_MAX; i++)
	  if (rmacs[i])
	       gcmark(rmacs[i]);
     /* Mark everything on the eval-stack. */
     register struct estack_entry *e=estack_top;
     register object **b;
     for ( ; e; e=e->prev) {
	  gcmark(e->exp);
	  gcmark(e->protected);
	  /* Most of these will be marked already, but better safe
	   * than sorry. There may be symbols bound that aren't in the
	   * obarray. */
	  for (b=e->bindings; b && *b; b+=2) {
	       gcmark(b[0]);
	       gcmark(b[1]);
	  }
     }
}

/* Walk the C stack (from gc()'s frame to that of tlinit()) and mark
 * objects pointed to from it. This is exceedingly hairy, since we
 * can't risk freeing something that shouldn't be freed, but neither
 * can we risk following random pointers that look convincing.

 * Most of what's important on the stack should be protected already,
 * since the eval-stack has been walked, but most routines don't make
 * entries in it (only eval(), apply(), catch() and call_closure()
 * do). Here we are mainly protecting C routines from nasty surprises.
 */
void gcstackwalk(void **bot)
{
     register void **o=bot;
     register object *p=*o;
     for ( ; o<gc_top_of_stack; o++, p=*o)
	  if (p && !INTP(p) && is_in_heap(p)) {
	       if (CONSP(p) || GC_CONSP(p))
		    gcmark( (object *) (((ptrbits_t) (p)) | 4) );
	       else if (TYPE(p) <= type_max)
		    gcmark(p);
	  }
}

/* The garbage collector */
DEFUN("gc", gc, 0, 0)
  () 
{
     void **s=GET_FRAME;
     if (INTVAL(SYMVAL(CVAR(max_objects))) < 1024) /* Reset this to a minimum. */
	  SYMVAL(CVAR(max_objects))=MKINT(1024);
     gcsillywalk();
     gcstackwalk(s);
     int freed=0, kept=0;
     struct blocklist_entry *b;
     for (b=blocklist_top; b; b=b->next) {
	  register object *o=b->block;
	  for ( ; o<b->nextfree; o++)
	       if (FREE_CELL_P(o)) {
		    continue;
	       } else if (!GCMARKED(o)) {
		    if (CAR(o) && CDR(o))
			 freeobj(o), freed++;
	       } else {
		    GCUNMARK(o), kept++;
	       }
     }
     return cons(MKINT(freed), MKINT(kept));
}

#include "include/mem-lispinit.h"

/* Initialise basic memory management stuff. */
void meminit()
{
     blocklist_top=blocklist_current
	  =xmalloc(sizeof(struct blocklist_entry)+(BLOCKSIZE*sizeof(object)));
     blocklist_top->nextfree=blocklist_top->block;
     freelistsize=BLOCKSIZE;
     initobarray(DEFOBARRAYSZ);
     protected_vars[2]=&obarray;
     protected_vars[0]=&s_in;
     protected_vars[1]=&s_out;
     protected_vars[PROT_VARS]=0;
     mem_lispinit();
}

void meminit2()
{
     /* We have to set this cell explicitly in the case of nil,
      * for obvious reasons. This is also why nil should be the first
      * symbol interned. */
     SYMPLIST(CVAR(nil))=CVAR(nil);
}
