#include "tl.h"
#include "print.h"

#include "exceptn.h"
#include "mem.h"
#include "stream.h"
#include "subr.h"
#include "types.h"

void tlprint(object *a, object *s);

void tlprint(object *a, object *s)
{
     ASSERT_TYPE(STREAMP(s), CVAR(streamp), s);
     ASSERT_TYPE(WSTRMP(s), CVAR(write_strm_p), s);
     if(!a) {
	  writs("<void>", s);
	  return;
     }
     a=GCSAFE(a);
     unsigned int t=TYPE(a);
     if (t > type_max) {	/* *Really* shouldn't happen... */
	  fprintf(stderr, "print: nasty type %d (0x%lx) - aborting.\n",
		  t, (ptrbits_t) a);
	  abort();		/* ... which is why we abort. */
     }
     switch(t) {
     case INTG :		/* There's no type_spec for ints. */
     {
	  char *istr=alloca(32);
	  snprintf(istr, 32, "%d", INTVAL(a)); /* 32 is more than enough. */
	  writs(istr, s);
	  break;
     }
     default :
	  if (types[t]->print)
	       types[t]->print(a, s);
	  else {
	       writs("#<", s);
	       writs(types[t]->name, s);
	       char *xstr=alloca(35);
	       snprintf(xstr, 35, " 0x%lx", (ptrbits_t) a);
	       writs(xstr, s);
	       writc('>', s);
	  }
	  break;
     }
}

DEFUN("print", print, 1, 2)
  (object *a, object *s)
{
     tlprint(a, s);
     putchar('\n');
     return a;
}

DEFUN("prin1", prin1, 1, 2)
  (object *a, object *s)
{
     tlprint(a, s);
     return a;
}

#include "include/print-lispinit.h"

void printinit()
{
     print_lispinit();
}
