#include "tl.h"
#include "eval.h"

#include "exceptn.h"
#include "subr.h"
#include "mem.h"
#include "types.h"
#include "print.h"
#include "stream.h"

DEFSYM("closure", CVAR(closure));
DEFVAR("backtrace-print-catch-points", CVAR(bt_print_cp), CVAR(nil));

void push_eval_stack(object *, int);
object *evlis(object *);
object *callsubr(object *, object *);
object *make_closure_vector();
object *call_closure(object *, object *);
object *call_lambda(object *, object *);
int check_args(object *, object *);
void bind_list(object *, object *);
object *spread_last(object *);

/* Stack of expressions being evaluated and active function-calls. */
struct obstack eval_stack;

struct estack_entry *estack_top=NULL;

struct estack_entry *estack_bot;

struct obstack saved_bindings;

enum frame_type { EVAL = 0, APPLY, CATCH, CLOSURE };

#define FRAMETYPE(f) ((f->type)&7)
#define LOCALS(f) (f->type&8 ? 1 : 0)
#define SETLOCALS(f) ((f)->type|=8)

void push_eval_stack(object *o, int type)
{
     struct estack_entry *e=obstack_alloc(&eval_stack,
					  sizeof(struct estack_entry));
     e->prev=estack_top;
     e->type=(char) type;
     if (!estack_top)
	  estack_bot=e;
     else if (LOCALS(estack_top) && type != APPLY)
	  SETLOCALS(e);
     estack_top=e;
     e->exp=o;
     e->protected=0;
     e->bindings=0;
}

void unwind_eval_stack(struct estack_entry *e)
{
     struct estack_entry *f=estack_top;
     int i;
     for ( ; f!=e; f=f->prev) {
	  object **b;
	  if (FRAMETYPE(f)==CLOSURE && f->exp)
	       for (i=0, b=VECREF(f->exp); i<ARRAYLEN(f->exp); i+=2)
		    b[i+1]=SYMVAL(b[i]);
	  if (f->protected)
	       progn(0, f->protected);
	  b=f->bindings;
	  if (b) {
	       for ( ; *b; b+=2)
		    SYMVAL(b[0])=b[1];
	       obstack_free(&saved_bindings, f->bindings);
	  }
 	  obstack_free(&eval_stack, f);
     }
     estack_top=e;
}

void pop_eval_stack()
{
     if (estack_top)
	  unwind_eval_stack(estack_top->prev);
}

DEFUN("cond", evcon, 1, UNEVALLED(ARGS_UNLIMITED))
  (int c, object *o)
{
     object *r;
     while(!NULLP(o)) {
	  ASSERT_TYPE(CONSP(car(o))||NULLP(car(o)), CVAR(listp), car(o));
	  if((r=eval(caar(o)))!=CVAR(nil))
	       return NULLP(cdar(o)) ?
		    r : eval(cadar(o));
	  else
	       o=cdr(o);
     }
     return CVAR(nil);
}

object *evlis(object *e)
{
     if (NULLP(e))
	  return CVAR(nil);
     object *r=cons(eval(car(e)), CVAR(nil));
     object *a=r;
     for (e=cdr(e); !NULLP(e); e=cdr(e), a=CDR(a))
	  CDR(a)=cons(eval(car(e)), CVAR(nil));
     return r;
}

DEFUN("lambda", lambda, 0, UNEVALLED(ARGS_UNLIMITED))
  (int c, object *a)
{
     return !(estack_top && LOCALS(estack_top)) ?
	  cons(CVAR(lambda), a) :
	  make_list(3, 
		    CVAR(closure), 
		    make_closure_vector(), 
		    cons(CVAR(lambda), a));
}

DEFUN("macro", macro, 0, UNEVALLED(ARGS_UNLIMITED))
  (int c, object *a)
{
     return cons(CVAR(macro), lambda(c-1, cdr(a)));
}

object *callsubr(object *s, object *a)
{
     s=indirect_function(s);
     int l=xlength(a), m=NUMMAXARGS(s), n=NUMMINARGS(s);

     if (l<n || (l>m && !UNLIMITED(s)))
	  throw3(CVAR(wrong_num_args), s, MKINT(l));
     object *ret;
     object * (*subr)()=OSUBR(s);
     if (UNLIMITED(s))
	  ret=(*subr)(l, a);
     else if (m==0)
	  ret=(*subr)();
     else {
	  object *args[m];		/* GNU C only (we should have a
					 * macro). */
	  l=0;
	  for ( ; l<m; a=cdr(a), l++)
	       args[l]=car(a);
	  switch (m) {		/* I can't think of a clever way to do this. */
	    case 1 :
		 ret=(*subr)(args[0]);
		 break;
	    case 2 :
		 ret=(*subr)(args[0], args[1]);
		 break;
	    case 3 :
		 ret=(*subr)(args[0], args[1], args[2]);
		 break;
	    case 4 :
		 ret=(*subr)(args[0], args[1], args[2], args[3]);
		 break;
	    default :
		 ret=(*subr)(args[0], args[1], args[2], args[3], args[4]);
	  }
     }
     return ret;
}

DEFUN("eval", eval, 1, 1)
  (object *e)
{
     push_eval_stack(e, EVAL);
     /* This shouldn't happen. If things are this badly screwed up,
      * we'll probably get a recursive error, and thus a core dump. */
     if(!e)
	  throw1(CVAR(void_obj));
     object *ret;
     switch (TYPE(e)) {
       case SYMB : 
	    ret=symbol_value(e);
	    break;
       case CONS :
       {
	    object *f=CAR(e);
	    f=indirect_function(f);
	    if (CONSP(f) && CAR(f)==CVAR(macro)) {
		 ret=eval(apply(2, make_list(2, cdr(f), CDR(e))));
		 break;
	    } else if (SUBRP(f) && SPECIAL(f)) {
		 ret=callsubr(CAR(e), CDR(e));
		 break;
	    } else {
		 ret=apply(2, make_list(2, CAR(e), evlis(CDR(e))));
		 break;
	    }
       }
       default :
	    ret=e;		/* Self-evaluating. */
	    break;
     }
     pop_eval_stack();
     return ret;
}

object *make_closure_vector()
{
     struct estack_entry *f=estack_top;
     object **b, **s;
     int x=1;
     for ( ; f; f=f->prev) {
	  if (!LOCALS(f))
	       break;
	  else if (!f->bindings)
	       continue;
	  for (x=1, b=f->bindings, s=obstack_base(&saved_bindings);
	       *b; b+=2, x=1, s=obstack_base(&saved_bindings))
	  {
	    for ( ; x && s<((object **)obstack_next_free(&saved_bindings)); s++)
		 if (s[0]==b[0])
			 x=0;
	       if (x) {
		    obstack_ptr_grow(&saved_bindings, b[0]);
		    obstack_ptr_grow(&saved_bindings, SYMVAL(b[0]));
	       }
	  }
     }
     x=obstack_object_size(&saved_bindings);
     b=obstack_base(&saved_bindings);
     object *ret=array_to_vector(x, obstack_finish(&saved_bindings));
     obstack_free(&saved_bindings, b);
     return ret;
}

object *call_closure(object *c, object *a)
{
     object *vec=car(c);
     push_eval_stack(vec, CLOSURE);
     SETLOCALS(estack_top);
     object **b=VECREF(vec);
     int i;
     for (i=0; i < ARRAYLEN(vec); i+=2) {
	  obstack_ptr_grow(&saved_bindings, b[i]);
	  obstack_ptr_grow(&saved_bindings, SYMVAL(b[i]));
	  SYMVAL(b[i])=b[i+1];
     }
     obstack_ptr_grow(&saved_bindings, NULL);
     estack_top->bindings=obstack_finish(&saved_bindings);
     object *ret=call_lambda(cadr(c), a);
     pop_eval_stack();
     return ret;
}

object *call_lambda(object *f, object *a)
{
     push_eval_stack(NULL, CLOSURE);
     object *parmlist;
     parmlist=cadr(f);
     if(!check_args(parmlist, a))
	  return throw3(CVAR(wrong_num_args), f, MKINT(xlength(a)));
     bind_list(parmlist, a);
     f=cddr(f);
     object *ret=progn(xlength(f), f);
     pop_eval_stack();
     return ret;
}

DEFUN("apply", apply, 1, ARGS_UNLIMITED)
  (int c, object *a)
{
     object *f=CAR(a);
     a=spread_last(a);
     push_eval_stack(a, APPLY);
     a=CDR(a);
     f=indirect_function(f);
     object *ret;
     if (CONSP(f)) {
	  if (CAR(f)==CVAR(lambda))
	       ret=call_lambda(f, a);
	  else if (CAR(f)==CVAR(closure))
	       ret=call_closure(cdr(f), a);
	  else
	       throw2(CVAR(invalid_func), f);
     } else if (SUBRP(f) && !SPECIAL(f)) {
	  ret=callsubr(f, a);
     } else
	  throw2(CVAR(invalid_func), f);
     pop_eval_stack();
     return ret;
}

DEFUN("funcall", funcall, 1, ARGS_UNLIMITED)
  (int c, object *a)
{
     return apply(2, cons(car(a), cons(cdr(a), CVAR(nil))));
}

/* Some special forms. */

DEFUN("prog1", prog1, 0, UNEVALLED(ARGS_UNLIMITED))
  (int c, object *v)
{
     object *ret=eval(car(v));
     for ( v=cdr(v); !NULLP(v); v=cdr(v))
	  eval(car(v));
     return ret;
}

DEFUN("progn", progn, 0, UNEVALLED(ARGS_UNLIMITED))
  (int c, object *a)
{
     object *ret=CVAR(nil);
     for ( ; !NULLP(a); a=cdr(a))
	  ret=eval(car(a));
     return ret;
}

/* Ensure that certain forms are evaluated when this frame is exited.
 * unwind_eval_stack() applies `progn' to non-void `protected' fields
 * when it's unwinding the stack. Local variable bindings will still
 * be in effect when these are evaluated. */
DEFUN("protect", protect, 1, UNEVALLED(ARGS_UNLIMITED))
  (int c, object *a)
{
     estack_top->protected=cdr(a);
     object *ret=eval(car(a));
     return ret;
}

DEFUN("let", let, 1, UNEVALLED(ARGS_UNLIMITED))
  (int c, object *a)
{
     push_eval_stack(NULL, CLOSURE);
     object *l=car(a);
     object *vars=CVAR(nil), *vals=CVAR(nil);
     for ( ; !NULLP(l); l=cdr(l))
	  if (!CONSP(car(l)))
	       vars=cons(car(l), vars), vals=cons(CVAR(nil), vals);
	  else
	       vars=cons(caar(l), vars), vals=cons(cadar(l), vals);
     bind_list(vars, evlis(vals));
     l=progn(c-1, cdr(a));
     pop_eval_stack();
     return l;
}

DEFUN("while", lisp_while, 1, UNEVALLED(ARGS_UNLIMITED))
  (int c, object *v)
{
     while (!NULLP(eval(car(v))))
	  progn(c-1, cdr(v));
     return CVAR(nil);
}

/* Check lambda-expression args against parameters. */
int check_args(object *parms, object *args)
{
     int opt=0;
     for ( ; !NULLP(parms); parms=cdr(parms), args=cdr(args))
	  if (car(parms)==CVAR(Amprest))
	       return 1;
	  else if (car(parms)==CVAR(Ampopt)) {
	       parms=cdr(parms); /* skip &optional */
	       opt=1;
	       continue;
	  } else if (NULLP(args))
	       return opt;
     return NULLP(args)?1:0;	/* Max. args exceeded? */
}

/* Bind a list of parameters to a list of values. Record the previous
 * values of the parameters in the `bindings' field of the current
 * frame. */
void bind_list(object *parmlist, object *vlist)
{
     if (NULLP(parmlist))
	  return;
     object *param, *val;
     for ( ; !NULLP(parmlist); parmlist=cdr(parmlist), vlist=cdr(vlist)) {
	  param=car(parmlist), val=car(vlist);
	  if (param==CVAR(Amprest)) {
	       param=cadr(parmlist);
	       parmlist=CVAR(nil);
	       val=vlist;
	  } else if (param==CVAR(Ampopt)) {
	       parmlist=cdr(parmlist);
	       param=car(parmlist);
	  }
	  ASSERT_TYPE(SYMBOLP(param), CVAR(symbolp), param);
	  if (CONST_SYM_P(param))
	       throw2(CVAR(const_sym), param);
	  obstack_ptr_grow(&saved_bindings, param);
	  obstack_ptr_grow(&saved_bindings, SYMVAL(param));
	  SYMVAL(param)=val;
     }
     obstack_ptr_grow(&saved_bindings, NULL);
     estack_top->bindings=obstack_finish(&saved_bindings);
     SETLOCALS(estack_top);
}

DEFUN("quote", quote, 1, UNEVALLED(1))
  (object *e)
{
     return e;
}

object *spread_last(object *list)
{
     if (NULLP(list))
	  return CVAR(nil);
     if (NULLP(cdr(list))) {
	  if (LISTP(car(list)))
	       return car(list);
	  else
	       throw3(CVAR(wrong_type), CVAR(listp), car(list));
     }
     object *aux;
     for (aux=list; !NULLP(cddr(aux)); aux=cdr(aux))
	  ;
     if (!LISTP(cadr(aux)))
	  throw3(CVAR(wrong_type), CVAR(listp), cadr(aux));
     setcdr(aux, cadr(aux));
     return list;
}
     
DEFUN("backtrace", btrace, 0, 1)
  (object *s)
{
     int c=0;
     if (!NULLP(SYMVAL(CVAR(bt_print_cp))))
	  c=1;
     struct estack_entry *e=estack_top;
     for ( ; e; e=e->prev) {
	  switch (FRAMETYPE(e)) {
	    case CATCH :
		 if (!c)
		      break;
		 writs("\t[catch-point ", s);
		 prin1(e->exp, s);
		 writs("]\n", s);
		 break;
	    case EVAL :
		 writs("\teval:\t", s);
		 print(e->exp, s);
		 break;
	    case APPLY :
		 writs("\tapply:\t", s);
		 prin1(car(e->exp), s);
		 writs(" to ", s);
		 if (NULLP(cdr(e->exp)))
		      writs("()\n", s);
		 else
		      print(cdr(e->exp), s);
		 break;
	  }
	  if (e->bindings) {
	       object **b=e->bindings;
	       for ( ; *b; b+=2) {
		    writs("\tbindings:\t", s);
		    prin1(b[0], s);
		    writs(" = ", s);
		    print(b[1], s);
	       }
	  }
     }
     return CVAR(nil);
}

#include "include/eval-lispinit.h"

void evalinit()
{
     obstack_init(&saved_bindings);
     obstack_init(&eval_stack);
     eval_lispinit();
}

void evalfinish()
{
     obstack_free(&saved_bindings, NULL);
     obstack_free(&eval_stack, NULL);
}
