#include "tl.h"
#include "subr.h"

#include "eval.h"
#include "exceptn.h"
#include "mem.h"
#include "types.h"

object *addsubr(struct subrinit s)
{
     object *r=intern(s.name);
     object *o=SYMFUNC(r)=new_object(SUBR);
     OSUBR(o)=s.func;
     SETARGS(o, s.args);
     SUBRNAME(o)=s.name;
     return r;
}

object *make_subr(object * (*func)(), int max, int min, char *name)
{
     object *ret=new_object(SUBR);
     OSUBR(ret)=func;
     SETARGS(ret, MINARGS(min)|MAXARGS(max));
     SUBRNAME(ret)=strdup(name);
     return ret;
}

DEFUN("atom", atom, 1, 1)
  (object *a)
{
     return !CONSP(a) ? CVAR(t) : CVAR(nil);
}

DEFUN("eq", eq, 2, 2)
  (object *a, object *b)
{
     return (a==b) ? CVAR(t) : CVAR(nil);
}

DEFUN("null", null, 1, 1)
  (object *a)
{
     return a==CVAR(nil) ? CVAR(t) : CVAR(nil);
}

DEFUN("consp", consp, 1, 1)
  (object *a)
{
     return CONSP(a)?CVAR(t):CVAR(nil);
}

DEFUN("listp", listp, 1, 1)
  (object *a)
{
     return (CONSP(a)||NULLP(a))?CVAR(t):CVAR(nil);
}

DEFUN("intp", intp, 1, 1)
  (object *a)
{
     return INTP(a)?CVAR(t):CVAR(nil);
}

DEFUN("symbolp", symbolp, 1, 1)
  (object *a)
{
     return SYMBOLP(a)?CVAR(t):CVAR(nil);
}

DEFUN("arrayp", arrayp, 1, 1)
  (object *a)
{
     return ARRAYP(a) ? CVAR(t) : CVAR(nil);
}

DEFUN("aref", aref, 2, 2)
  (object *a, object *i)
{
     ASSERT_TYPE(ARRAYP(a), CVAR(arrayp), a);
     ASSERT_TYPE(INTP(i), CVAR(intp), i);
     if (INTVAL(i)>=ARRAYLEN(a))
	  throw2(CVAR(index_oob), i);
     return STRINGP(a) ?
	  MKINT(ARR_PTR(a)->type.string[INTVAL(i)]) :
	  ARR_PTR(a)->type.vector[INTVAL(i)];
}

DEFUN("aset", aset, 3, 3)
  (object *a, object *i, object *o)
{
     ASSERT_TYPE(ARRAYP(a), CVAR(arrayp), a);
     ASSERT_TYPE(INTP(i), CVAR(intp), i);
     if (STRINGP(a))
	  ASSERT_TYPE(INTP(o), CVAR(intp), o);
     int j=INTVAL(i);
     if (j>=ARRAYLEN(a))
	  throw2(CVAR(index_oob), i);
     if (STRINGP(a))
	  STRREF(a)[j]=INTVAL(o);
     else 
	  VECREF(a)[j]=o;
     return o;
}

DEFUN("cons", cons, 2, 2)
  (object *a,object *b)
{
     object *ret=new_object(CONS);
     if(ret)
	  CAR(ret)=a, CDR(ret)=b;
     return ret;
}

DEFUN("car", car, 1, 1)
  (object *a)
{
     if(NULLP(a))
	  return CVAR(nil);
     ASSERT_TYPE(CONSP(a), CVAR(listp), a);
     return CAR(a);
}

DEFUN("cdr", cdr, 1, 1)
  (object *a)
{
     if(NULLP(a))
	  return CVAR(nil);
     ASSERT_TYPE(CONSP(a), CVAR(listp), a);
     return CDR(a);
}

DEFUN("setcar", setcar, 2, 2)
  (object *c, object *o)
{
     ASSERT_TYPE(CONSP(c), CVAR(consp), c);
     CAR(c)=o;
     return o;
}

DEFUN("setcdr", setcdr, 2, 2)
  (object *c, object *o)
{
     ASSERT_TYPE(CONSP(c), CVAR(consp), c);
     CDR(c)=o;
     return o;
}
     
DEFUN("stringp", stringp, 1, 1)
  (object *s)
{
     return STRINGP(s) ? CVAR(t) : CVAR(nil);
}

DEFUN("vectorp", vectorp, 1, 1)
  (object *v)
{
     return VECTORP(v) ? CVAR(t) : CVAR(nil);
}

DEFUN("sequencep", sequencep, 1, 1)
  (object *s)
{
     return LISTP(s) || ARRAYP(s) ? CVAR(t) : CVAR(nil);
}

DEFUN("functionp", functionp, 1, 1)
  (object *f)
{
     if (SYMBOLP(f))
	  return fboundp(f);
     return SUBRP(f)
	  || (CONSP(f) && (CAR(f)==CVAR(lambda) || CAR(f)==CVAR(macro)
			   || CAR(f)==CVAR(closure)))
	  ? CVAR(t) : CVAR(nil);
}

DEFUN("set", set, 2, 2)
  (object *a,object *to) 
{
     ASSERT_TYPE(SYMBOLP(a), CVAR(symbolp), a);
     if(CONST_SYM_P(a))
	  throw2(CVAR(const_sym), a);
     SYMVAL(a)=to;
     return to;
}

DEFUN("boundp", boundp, 1, 1)
  (object *a)
{
     ASSERT_TYPE(SYMBOLP(a), CVAR(symbolp), a);
     return SYMVAL(a)?CVAR(t):CVAR(nil);
}

DEFUN("makunbound", makunbound, 1, 1)
  (object *a)
{
     ASSERT_TYPE(SYMBOLP(a), CVAR(symbolp), a);
     SYMVAL(a)=0;
     return a;
}

DEFUN("fboundp", fboundp, 1, 1)
  (object *a)
{
     ASSERT_TYPE(SYMBOLP(a), CVAR(symbolp), a);
     return SYMFUNC(a)?CVAR(t):CVAR(nil);
}

DEFUN("fmakunbound", fmakunbound, 1, 1)
  (object *a)
{
     ASSERT_TYPE(SYMBOLP(a), CVAR(symbolp), a);
     SYMFUNC(a)=0;
     return a;
}

DEFUN("fset", fset, 2, 2)
  (object *a,object *to)
{
     ASSERT_TYPE(SYMBOLP(a), CVAR(symbolp), a);
     SYMFUNC(a)=to;
     return to;
}

DEFUN("+", plus, 0, ARGS_UNLIMITED)
  (int c, object *a)
{
     int ret=0;
     object *o;
     while (!NULLP(a)) {
	  o=car(a);
	  ASSERT_TYPE(INTP(o), CVAR(intp), o);
	  ret+=INTVAL(o);
	  a=cdr(a);
     }
     return MKINT(ret);
}

DEFUN("-", minus, 0, ARGS_UNLIMITED)
  (int c, object *a)
{
     if(NULLP(a))
	  return MKINT(0);
     object *o=car(a);
     a=cdr(a);
     ASSERT_TYPE(INTP(o), CVAR(intp), o);
     if (NULLP(a))
	  return MKINT(-(INTVAL(o)));
     int ret=INTVAL(o);
     while(!NULLP(a)) {
	  o=car(a);
	  ASSERT_TYPE(INTP(o), CVAR(intp), o);
	  ret-=INTVAL(o);
	  a=cdr(a);
     }
     return MKINT(ret);
}

DEFUN("*", mul, 0, ARGS_UNLIMITED)
  (int c, object *a)
{
     int ret=1;
     object *o;
     while (!NULLP(a)) {
	  o=car(a);
	  ASSERT_TYPE(INTP(o), CVAR(intp), o);
	  ret*=INTVAL(o);
	  a=cdr(a);
     }
     return MKINT(ret);
}

DEFUN("/", divide, 1, ARGS_UNLIMITED)
  (int c, object *a)
{
     return MKINT( INTVAL(car(a)) / INTVAL(mul(c-1, cdr(a))) );
}

DEFUN("symbol-function", symbol_function, 1, 1)
  (object *a)
{
     ASSERT_TYPE(SYMBOLP(a), CVAR(symbolp), a);
     object *ret=SYMFUNC(a);
     if(!ret)
	  throw2(CVAR(void_func), a);
     return ret;
}

DEFUN("indirect-function", indirect_function, 1, 1)
  (object *a)
{
     while (SYMBOLP(a))
	  a=symbol_function(a);
     return a;
}

object *make_list(int c,...)
{
     if(c==0)
	  return CVAR(nil);
     va_list l;
     va_start(l, c);
     object *ret=cons(va_arg(l, object *), CVAR(nil));
     object *aux=ret;
     while(--c) {
	  CDR(aux)=cons(va_arg(l, object *), CVAR(nil));
	  aux=CDR(aux);
     }
     va_end(l);
     return ret;
}

DEFUN("list", list, 1, ARGS_UNLIMITED)
  (int c, object *v)
{
     if(c==0)
	  return CVAR(nil);
     return cons(car(v), list(--c, cdr(v)));
}

DEFUN("memq", memq, 2, 2)
  (object *o, object *l)
{
     ASSERT_TYPE(LISTP(l), CVAR(listp), l);
     for ( ; !NULLP(l); l=cdr(l))
	  if (car(l)==o)
	       return l;
     return CVAR(nil);
}

DEFUN("symbol-value", symbol_value, 1, 1)
  (object *a)
{
     ASSERT_TYPE(SYMBOLP(a), CVAR(symbolp), a);
     object *ret=SYMVAL(a);
     if(!ret)
	  throw2(CVAR(void_var), a);
     return ret;
}

DEFUN("symbol-name", symbol_name, 1, 1)
  (object *a)
{
     ASSERT_TYPE(SYMBOLP(a), CVAR(symbolp), a);
     return make_string(SYMNAME(a));
}

DEFUN("symbol-plist", symbol_plist, 1, 1)
  (object *a)
{
     ASSERT_TYPE(SYMBOLP(a), CVAR(symbolp), a);
     return SYMPLIST(a);
}

DEFUN("get", get, 2, 2)
  (object *sym, object *prp)
{
     ASSERT_TYPE(SYMBOLP(sym), CVAR(symbolp), sym);
     object *o=memq(prp, SYMPLIST(sym));
     if (NULLP(o))
	  return CVAR(nil);
     else
	  return cadr(o);
}

DEFUN("put", put, 3, 3)
  (object *sym, object *prp, object *val)
{
     ASSERT_TYPE(SYMBOLP(sym), CVAR(symbolp), sym);
     object *o=memq(prp, SYMPLIST(sym));
     if (NULLP(o))
	  o=SYMPLIST(sym)=cons(prp, cons(CVAR(nil), SYMPLIST(sym)));
     setcar(cdr(o), val);
     return val;
}

DEFUN("length", length, 1, 1)
  (object *s)
{
     ASSERT_TYPE(LISTP(s) || ARRAYP(s), CVAR(sequencep), s);
     return MKINT(LISTP(s) ? xlength(s) : ARRAYLEN(s));
}

DEFUN("defmacro", defmacro, 2, UNEVALLED(ARGS_UNLIMITED))
  (int c, object *a)
{
     fset(car(a), cons(CVAR(macro), lambda(0, cdr(a))));
     return car(a);
}

DEFUN("defun", defun, 2, UNEVALLED(ARGS_UNLIMITED))
  (int c, object *a)
{
     fset(car(a), lambda(0, cdr(a)));
     return car(a);
}

DEFUN("exit", tlexit, 0, 1)
  (object *c)
{
     exit(NULLP(c) ? 0 : INTVAL(c));
}

unsigned long xlength(object *a)
{
     if(!(NULLP(a)||CONSP(a)))
	  return 0;
     int i=0;
     while(!NULLP(a))
	  a=cdr(a), i++;
     return i;
}

#include "include/subr-lispinit.h"

void initsubrs()
{
     subr_lispinit();
}
