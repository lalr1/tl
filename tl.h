#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <limits.h>
#include <setjmp.h>
#include <stdarg.h>
#include <mcheck.h>
#include <obstack.h>
#include <stddef.h>

#define obstack_chunk_alloc xmalloc
#define obstack_chunk_free free

enum type_code { SUBR =1, SYMB, CONS, INTG, STRM, ARRAY };

#define NTYPES ARRAY

#define DEFOBARRAYSZ 211
#define BLOCKSIZE 1024
#define PROT_VARS 3

#define DEFSYM(lname, cname) 
#define DEFVAR(lname, cname, val)

#define DEFERRSYM(lname, cname)

#define CVAR(name) C_ ## name

#define DEFUN(lname, cname, minargs, maxargs)				\
     struct subrinit cname ## _struct = { lname, cname,			\
					  MINARGS(minargs)|MAXARGS(maxargs) }; \
object * cname


/* Type system. */
/* #define CRUFT +1.0eINF */

typedef unsigned long ptrbits_t;

#define GCBIT 2
#define GCMARKED(o) ((((ptrbits_t) CAR(o)) & GCBIT)?1:0)

/* Cons bit has to be masked out before using these. */
#define GCMARK(o) o->type |= GCBIT
#define GCUNMARK(o) o->type &= ~GCBIT

#define TYPE(o) (INTP(o)? INTG : CONSP(o) ? CONS : TYPE_TAG(o))
#define SETTYPE(o, i) (o)->type=(i==CONS ? 0 : ((((i&0xfff)<<4)|1)))

#define MKINT(i) ((object *) ((((long)i)<<4) | 5))
#define INTVAL(i) ((int) (((ptrbits_t) i)>>4))

#define IMMEDP(o) ((((ptrbits_t) (o)) & 5) == 5)
#define NINTIMP(o) ((((ptrbits_t) (o)) & 0xd) == 0xd)
#define MKNIMMED(i) ((((int) (i)) << 4) & 0xd)

#define OBJP(o) ((((ptrbits_t) CAR(o)) & 0xd) == 1)
#define SET_FREE_CELL(c) CAR(c) = (struct object *) 9
#define FREE_CELL_P(c) ((((ptrbits_t) CAR(c)) & 0xd) == 9)
#define TYPE_TAG(o) (((o)->type>>4) & 0xfff)
#define INTP(e) ((((ptrbits_t) (e)) & 0xd) == 5)
#define SYMBOLP(e) (TYPE(e)==SYMB)
#define NULLP(e) ((e)==CVAR(nil))
#define CONSP(e) ((((ptrbits_t) (e) ) & 5) == 4)
#define SUBRP(e) (TYPE(e)==SUBR)
#define LISTP(o) (CONSP(o) || NULLP(o))

#define INTRNBIT 1<<16
#define SYMCONSTBIT 1<<17
#define SYM_INTERNED_P(s) ((s)->type & INTRNBIT)
#define SET_INTERNED(s) ((s)->type|=INTRNBIT)
#define SET_N_INTERNED(s) ((s)->type&=~INTRNBIT)
#define CONST_SYM_P(s) ((s)->type&SYMCONSTBIT)
#define SET_CONST(s) ((s)->type|=SYMCONSTBIT)
#define SYMNAME(o) (o)->ptr.sym->symname
#define SYMVAL(o) (o)->ptr.sym->symval
#define SYMFUNC(o) (o)->ptr.sym->symfunc
#define SYMPLIST(o) (o)->ptr.sym->plist

#define CONS_PTR(o) ((struct cons *)((ptrbits_t)(o)&~6))
#define CAR(o) (CONS_PTR(o)->car)
#define CDR(o) (CONS_PTR(o)->cdr)

#define caar(o) car(car(o))
#define cdar(o) cdr(car(o))
#define cadr(o) car(cdr(o))  
#define cddr(o) cdr(cdr(o))
#define caddr(o) car(cdr(cdr(o)))
#define cadar(o) car(cdr(car(o)))

#define throw1(o) throw(1, cons(o, CVAR(nil)))
#define throw2(o, p) throw(2, make_list(2, o, p))
#define throw3(o, p, q) throw(3, make_list(3, o, p, q))

#define ASSERT_TYPE(test, type, obj)				\
if (!(test))							\
  throw3(CVAR(wrong_type), type, obj)


/* Here are the major structures used to represent Lisp objects
 */

struct subr {
     struct object * (*subr)();	/* the C function */
     char *subrname;		/*  `subrname' is the char * passed to */
};				/*  addsubr() */

struct symbol {
     char *symname;
     struct object *symval;
     struct object *symfunc;
     struct object *plist;
};

struct cons {
     struct object *car;
     struct object *cdr;
};

struct stream {
     char *name;
     union {
	  FILE *file;
     } type;
};

struct array {
     unsigned int length;
     union {
	  char *string;
	  struct object **vector;
     } type;
};

/* Lisp objects are handled by C code as pointers to structures of the
 * following type. They are allocated in large blocks. See objalloc()
 * in mem.c. Effectively, they represent a header for the object.
 * Conses don't have this header -- they are allocated as 8-byte
 * chunks just like `struct object's */


typedef struct object {
     unsigned long type;
     union {
	  struct subr *subr;
	  struct symbol *sym;
	  struct array *array;
	  struct stream *stream;
	  void *any;
     } ptr;
} object;

