#include "tl.h"

#include "eval.h"
#include "exceptn.h"
#include "stream.h"
#include "print.h"
#include "read.h"
#include "subr.h"
#include "mem.h"
#include "types.h"

#include "include/exceptn-lispsyms.h"
#include "include/mem-lispsyms.h"
#include "include/read-lispsyms.h"

jmp_buf abort_to_repl;

int tlinit();
int main();

int tlinit()
{
     if(atexit(freeall)) {
	  puts("atexit failed");
	  return EXIT_FAILURE;
     }
     typeinit();
     meminit();
     meminit2();
     streaminit();
     evalinit();
     initsubrs();
     printinit();
     readinit();
     siginit();

     gc_top_of_stack=__builtin_frame_address(0);
     return EXIT_SUCCESS;
}

int main()
{
     tlinit();
     set_def_handler(&abort_to_repl);
     put(CVAR(eof), CVAR(handler), CVAR(handler_exit));
     setjmp(abort_to_repl);
     while(1)
	  print(eval(tlread(s_in, CVAR(nil))), s_out);
     return 0;
}
