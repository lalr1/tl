#include "tl.h"
#include "read.h"

#include "eval.h"
#include "exceptn.h"

#include "mem.h"
#include "print.h"
#include "stream.h"
#include "subr.h"
#include "types.h"

typedef object * (read_macro)(object *, object *);

/* Eat whitespace and comments until next char. */
#define EAT_SPACE(c, s)					\
while (c!=EOF && (isspace(c=readc(s)) || c==';'))	\
  if (c==';')						\
  while ((c=readc(s))!='\n')

#define EAT_SPACE_EOF(c, s);			\
EAT_SPACE(c, s);				\
if (c==EOF) throw1(C_eof_during_read)

/* Read-buffer. */
char *read_buf;
int read_buf_size;

object *read_sym(int, object *);
object *set_macro_char(object *, object *);
object *get_macro_char(object *);
void put_read_buf(int, int);
object *read_cons(object *, object *);
object *lex_num(char *);
object *read_string(object *, object *);
object *read_char(object *, object *);
object *read_vector(object *, object *);
object *read_quote(object *, object *);
object *ret0(object *, object *);
object *add_C_read_macro(int, read_macro, char *);

DEFSYM("eof", CVAR(eof));

/* Function to grow the read-buffer if necessary. Replace this with an
 * obstack, perhaps? It may be slightly more efficient this way, but
 * then again it may not. */
void put_read_buf(int c, int i)
{
     if (i>=read_buf_size)
	  read_buf=xrealloc(read_buf, read_buf_size+=read_buf_size);
     read_buf[i]=(char) c;
}

DEFUN("read", tlread, 0, 2)
  (object *s, object *eof_err_p)
{
     int c=0;
     EAT_SPACE(c, s);
     if (c==EOF) {
	  if (!NULLP(eof_err_p))
	       throw1(CVAR(eof_during_read)); /* This is considered an error. */
	  else
	       throw1(CVAR(eof));	/* This isn't. */
     }
     c&=UCHAR_MAX;
     object *ret;
     if (rmacs[c])
	  if (SUBRP(rmacs[c]))
	       ret=(*(OSUBR(rmacs[c])))(MKINT(c), s);
	  else
	       ret=funcall(3, make_list(3, rmacs[c], MKINT(c), s));
     else
	  ret=read_sym(c, s);
     /* Rudimentary syntax check. */
     if (!ret)
	  throw2(CVAR(invalid_syntax), MKINT(c));
     return ret;
}

object *read_cons(object *i, object *s)
{
     int c=0;
     EAT_SPACE_EOF(c, s);
     if (c==')')	/* End of list. */
	  return CVAR(nil);
     unreadc(c, s);
     object *rcar=tlread(s, CVAR(t));
     EAT_SPACE_EOF(c, s);
     if (c=='.') {
	  object *r=cons(rcar, tlread(s, CVAR(t)));
	  EAT_SPACE_EOF(c, s);	/* Clean up after ourselves. */
	  return r;
     } else {
	  unreadc(c, s);
	  return cons(rcar, read_cons(MKINT(c), s));
     }
}

/* Decide whether a string represents a number. If any character is
 * not a digit, bar an optional initial `+' or `-', we must be dealing
 * with a symbol, so return 0. */
object *lex_num(char *s)
{
     char *n=s;
     if (n[0]=='+' || n[0]=='-')
	  n++;
     if (!(*n))
	  return 0;
     for ( ; *n; n++)
	  if (!isdigit(*n))
	       return 0;
     return MKINT(strtol(s, NULL, 10));
}

object *read_sym(int c, object *s)
{
     int i=0, esc=0, maybe_num=1;
     if (!(c=='+' || c=='-' || isdigit(c)))
	  maybe_num=0;
     for ( ; c!=EOF && (esc || !(isspace(c) || rmacs[c])); c=readc(s))
	  if (c=='\\') 
	       esc=1, maybe_num=0;
	  else
	       esc=0, put_read_buf(c, i++);
     if (esc && c==EOF)
	  throw1(CVAR(eof_during_read));
     put_read_buf('\0', i);
     unreadc(c, s);
     object *r=0;
     if (maybe_num)
	  r=lex_num(read_buf);
     if (r)
	  return r;
     else
	  return intern(read_buf);
}

object *read_string(object *c, object *s)
{
     int h=0, i=0;
     for (h=readc(s); h!='"'; h=readc(s), i++)
	  put_read_buf(h, i);
     put_read_buf('\0', i);
     return make_string(read_buf);
}
     
object *read_char(object *c, object *s)
{
     int r=readc(s);
     if (r==EOF)
	  throw1(CVAR(eof_during_read));
     return MKINT(r);
}

object *read_vector(object *c, object *s)
{
     int sz=32, a=0;
     object **b=xmalloc(sz*sizeof(object *));
     int t;
     EAT_SPACE_EOF(t, s);
     for ( ; t!=']'; a++) {
	  unreadc(t, s);
	  if (a==sz)
	       b=xrealloc(b, (sz+=sz)*sizeof(object *));
	  b[a]=tlread(s, CVAR(t));
	  EAT_SPACE_EOF(t, s);
     }
     b=xrealloc(b, a*sizeof(object *));

     object *ret=array_to_vector(a*sizeof(object *), b);
     free(b);
     return ret;
}

/* This could easily be done from Lisp, but it's probably a good deal
 * faster in C, and more convenient until we have init-file
 * support. */
object *read_quote(object *c, object *s)
{
     return make_list(2, CVAR(quote), tlread(s, CVAR(t)));
}

/* No-op for special characters that don't need a macro. Giving this
 * as their definition simplifies checking for special (i.e. meta-)
 * characters like '.' in other routines, and lets `read' do some
 * basic syntax-checking. */
object *ret0(object *c, object *s)
{
     return 0;
}

/* Add a read-macro from Lisp. As in:
 *   (set-macro-char ?' (lambda (c s) (list (quote quote) (read s t)))) */
/* The function must, like C read-macros, take two arguments: the
 * character read and the stream to read from. The read-macro itself
 * is not checked at all.
 */
DEFUN("set-macro-char", set_macro_char, 2, 2)
  (object *c, object *f)
{
     ASSERT_TYPE(INTP(c), CVAR(intp), c);
     rmacs[(char) INTVAL(c)]=f;
     return f;
}

/* Get the read-macro associated with a character, or nil if there is
 * none. */
DEFUN("get-macro-char", get_macro_char, 1, 1)
  (object *c)
{
     ASSERT_TYPE(INTP(c), CVAR(intp), c);
     object *ret=rmacs[(char) INTVAL(c)];
     if (!ret)
	  return CVAR(nil);
     else
	  return ret;
}

/* Wrap up a C function in a subr object, so it can be called through
 * `funcall'. This is probably unnecessary and slow, but it's easier
 * (for now). */
object *add_C_read_macro(int c, read_macro r, char *name)
{
     object *s=new_object(SUBR);
     OSUBR(s)=r ? r : ret0;
     SUBRNAME(s)=name ? name : "read-macro";
     SETARGS(s, MAXARGS(2)|MINARGS(2));
     rmacs[c]=s;
     return s;
}

#include "include/read-lispinit.h"

void readinit()
{
     rmacs=(object **) xmalloc(256*sizeof(object *));
     read_buf=xmalloc(256*sizeof(char));
     read_buf_size=256*sizeof(char);
     memset(rmacs, 0, 256*sizeof(object *));

     add_C_read_macro('(', read_cons, "read-cons");

     /* Emacs syntax for characters. */
     add_C_read_macro('?', read_char, "read-char");
     add_C_read_macro('\'', read_quote, "read-quote");
     add_C_read_macro('"', read_string, "read-string");

     /* ... and for vectors. */
     add_C_read_macro('[', read_vector, "read-vector");

     /* No-op macros, for delimiters etc. that don't need macros of
      * their own. */
     object *m=add_C_read_macro(')', NULL, "read-delimiter");
     rmacs['.']=rmacs[']']=m;
     read_lispinit();
}

void readfinish(void)
{
     free(read_buf);
     free(rmacs);
}
