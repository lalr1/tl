#include "read-lispsyms.h"
#include "read-lispdecls.h"
#define ADDSUBR(name) CVAR(name)=addsubr(name ## _struct)

void read_lispinit ()
{
# 1 "<stdin>"
# 1 "<built-in>"
# 1 "<command-line>"
# 1 "/usr/include/stdc-predef.h" 1 3 4
# 1 "<command-line>" 2
# 1 "<stdin>"
CVAR(eof) = intern("eof");
ADDSUBR(tlread);
ADDSUBR(set_macro_char);
ADDSUBR(get_macro_char);
}
