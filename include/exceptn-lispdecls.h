object * def_handler
  (object *, object *)
;
object * catch
  (object *, object *, object *)
;
object * throw
  (int, object *)
;
object * handler_exit
  (int, object *)
;
object * handler_abort
  (int, object *)
;
