object * atom
  (object *)
;
object * eq
  (object *, object *)
;
object * null
  (object *)
;
object * consp
  (object *)
;
object * listp
  (object *)
;
object * intp
  (object *)
;
object * symbolp
  (object *)
;
object * arrayp
  (object *)
;
object * aref
  (object *, object *)
;
object * aset
  (object *, object *, object *)
;
object * cons
  (object *,object *)
;
object * car
  (object *)
;
object * cdr
  (object *)
;
object * setcar
  (object *, object *)
;
object * setcdr
  (object *, object *)
;
object * stringp
  (object *)
;
object * vectorp
  (object *)
;
object * sequencep
  (object *)
;
object * functionp
  (object *)
;
object * set
  (object *,object *)
;
object * boundp
  (object *)
;
object * makunbound
  (object *)
;
object * fboundp
  (object *)
;
object * fmakunbound
  (object *)
;
object * fset
  (object *,object *)
;
object * plus
  (int, object *)
;
object * minus
  (int, object *)
;
object * mul
  (int, object *)
;
object * divide
  (int, object *)
;
object * symbol_function
  (object *)
;
object * indirect_function
  (object *)
;
object * list
  (int, object *)
;
object * memq
  (object *, object *)
;
object * symbol_value
  (object *)
;
object * symbol_name
  (object *)
;
object * symbol_plist
  (object *)
;
object * get
  (object *, object *)
;
object * put
  (object *, object *, object *)
;
object * length
  (object *)
;
object * defmacro
  (int, object *)
;
object * defun
  (int, object *)
;
object * tlexit
  (object *)
;
