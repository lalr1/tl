object * streamp
  (object *)
;
object * read_strm_p
  (object *)
;
object * write_strm_p
  (object *)
;
object * get_curr_out
  ()
;
object * set_output
  (object *)
;
object * get_curr_in
  ()
;
object * set_input
  (object *)
;
object * write_char
  (object *, object *)
;
object * read_ch
  (object *)
;
object * unread_char
  (object *, object *)
;
