#include "exceptn-lispsyms.h"
#include "exceptn-lispdecls.h"
#define ADDSUBR(name) CVAR(name)=addsubr(name ## _struct)

void exceptn_lispinit ()
{
# 1 "<stdin>"
# 1 "<built-in>"
# 1 "<command-line>"
# 1 "/usr/include/stdc-predef.h" 1 3 4
# 1 "<command-line>" 2
# 1 "<stdin>"
CVAR(errors) = intern("errors");
CVAR(error) = intern("error");
CVAR(void_func) = intern("void-function"); put(CVAR(void_func), CVAR(errors), make_list(2, CVAR(void_func), CVAR(error)));
CVAR(void_var) = intern("void-variable"); put(CVAR(void_var), CVAR(errors), make_list(2, CVAR(void_var), CVAR(error)));
CVAR(void_obj) = intern("void-object"); put(CVAR(void_obj), CVAR(errors), make_list(2, CVAR(void_obj), CVAR(error)));
CVAR(wrong_type) = intern("wrong-type"); put(CVAR(wrong_type), CVAR(errors), make_list(2, CVAR(wrong_type), CVAR(error)));
CVAR(const_sym) = intern("set-constant-symbol"); put(CVAR(const_sym), CVAR(errors), make_list(2, CVAR(const_sym), CVAR(error)));
CVAR(wrong_num_args) = intern("wrong-number-of-args"); put(CVAR(wrong_num_args), CVAR(errors), make_list(2, CVAR(wrong_num_args), CVAR(error)));
CVAR(invalid_func) = intern("invalid-function"); put(CVAR(invalid_func), CVAR(errors), make_list(2, CVAR(invalid_func), CVAR(error)));
CVAR(invalid_syntax) = intern("invalid-read-syntax"); put(CVAR(invalid_syntax), CVAR(errors), make_list(2, CVAR(invalid_syntax), CVAR(error)));
CVAR(index_oob) = intern("index-out-of-bounds"); put(CVAR(index_oob), CVAR(errors), make_list(2, CVAR(index_oob), CVAR(error)));
CVAR(eof_during_read) = intern("eof-during-read"); put(CVAR(eof_during_read), CVAR(errors), make_list(2, CVAR(eof_during_read), CVAR(error)));
CVAR(uncaught_excptn) = intern("uncaught-exception"); put(CVAR(uncaught_excptn), CVAR(errors), make_list(2, CVAR(uncaught_excptn), CVAR(error)));
CVAR(file_error) = intern("file-error"); put(CVAR(file_error), CVAR(errors), make_list(2, CVAR(file_error), CVAR(error)));
CVAR(handler) = intern("handler");
CVAR(abort_recur_err) = intern("abort-on-recursive-error"); SYMVAL(CVAR(abort_recur_err)) = CVAR(t);
ADDSUBR(def_handler);
ADDSUBR(catch);
ADDSUBR(throw);
ADDSUBR(handler_exit);
ADDSUBR(handler_abort);
}
