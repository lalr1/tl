#include "eval-lispsyms.h"
#include "eval-lispdecls.h"
#define ADDSUBR(name) CVAR(name)=addsubr(name ## _struct)

void eval_lispinit ()
{
# 1 "<stdin>"
# 1 "<built-in>"
# 1 "<command-line>"
# 1 "/usr/include/stdc-predef.h" 1 3 4
# 1 "<command-line>" 2
# 1 "<stdin>"
CVAR(closure) = intern("closure");
CVAR(bt_print_cp) = intern("backtrace-print-catch-points"); SYMVAL(CVAR(bt_print_cp)) = CVAR(nil);
ADDSUBR(evcon);
ADDSUBR(lambda);
ADDSUBR(macro);
ADDSUBR(eval);
ADDSUBR(apply);
ADDSUBR(funcall);
ADDSUBR(prog1);
ADDSUBR(progn);
ADDSUBR(protect);
ADDSUBR(let);
ADDSUBR(lisp_while);
ADDSUBR(quote);
ADDSUBR(btrace);
}
