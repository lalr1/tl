#include "mem-lispsyms.h"
#include "mem-lispdecls.h"
#define ADDSUBR(name) CVAR(name)=addsubr(name ## _struct)

void mem_lispinit ()
{
# 1 "<stdin>"
# 1 "<built-in>"
# 1 "<command-line>"
# 1 "/usr/include/stdc-predef.h" 1 3 4
# 1 "<command-line>" 2
# 1 "<stdin>"
CVAR(nil) = intern("nil"); SYMVAL(CVAR(nil)) = CVAR(nil);
CVAR(t) = intern("t"); SYMVAL(CVAR(t)) = CVAR(t);
CVAR(Ampopt) = intern("&optional");
CVAR(Amprest) = intern("&rest");
CVAR(max_objects) = intern("max-objects"); SYMVAL(CVAR(max_objects)) = MKINT(16384);
ADDSUBR(heap_info);
ADDSUBR(gensym);
ADDSUBR(lisp_make_sym);
ADDSUBR(lisp_intern);
ADDSUBR(uninternatom);
ADDSUBR(gc);
}
