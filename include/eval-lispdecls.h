object * evcon
  (int, object *)
;
object * lambda
  (int, object *)
;
object * macro
  (int, object *)
;
object * eval
  (object *)
;
object * apply
  (int, object *)
;
object * funcall
  (int, object *)
;
object * prog1
  (int, object *)
;
object * progn
  (int, object *)
;
object * protect
  (int, object *)
;
object * let
  (int, object *)
;
object * lisp_while
  (int, object *)
;
object * quote
  (object *)
;
object * btrace
  (object *)
;
