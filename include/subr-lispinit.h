#include "subr-lispsyms.h"
#include "subr-lispdecls.h"
#define ADDSUBR(name) CVAR(name)=addsubr(name ## _struct)

void subr_lispinit ()
{
# 1 "<stdin>"
# 1 "<built-in>"
# 1 "<command-line>"
# 1 "/usr/include/stdc-predef.h" 1 3 4
# 1 "<command-line>" 2
# 1 "<stdin>"
ADDSUBR(atom);
ADDSUBR(eq);
ADDSUBR(null);
ADDSUBR(consp);
ADDSUBR(listp);
ADDSUBR(intp);
ADDSUBR(symbolp);
ADDSUBR(arrayp);
ADDSUBR(aref);
ADDSUBR(aset);
ADDSUBR(cons);
ADDSUBR(car);
ADDSUBR(cdr);
ADDSUBR(setcar);
ADDSUBR(setcdr);
ADDSUBR(stringp);
ADDSUBR(vectorp);
ADDSUBR(sequencep);
ADDSUBR(functionp);
ADDSUBR(set);
ADDSUBR(boundp);
ADDSUBR(makunbound);
ADDSUBR(fboundp);
ADDSUBR(fmakunbound);
ADDSUBR(fset);
ADDSUBR(plus);
ADDSUBR(minus);
ADDSUBR(mul);
ADDSUBR(divide);
ADDSUBR(symbol_function);
ADDSUBR(indirect_function);
ADDSUBR(list);
ADDSUBR(memq);
ADDSUBR(symbol_value);
ADDSUBR(symbol_name);
ADDSUBR(symbol_plist);
ADDSUBR(get);
ADDSUBR(put);
ADDSUBR(length);
ADDSUBR(defmacro);
ADDSUBR(defun);
ADDSUBR(tlexit);
}
