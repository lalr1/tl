object * CVAR(errors);
object * CVAR(error);
object * CVAR(void_func);
object * CVAR(void_var);
object * CVAR(void_obj);
object * CVAR(wrong_type);
object * CVAR(const_sym);
object * CVAR(wrong_num_args);
object * CVAR(invalid_func);
object * CVAR(invalid_syntax);
object * CVAR(index_oob);
object * CVAR(eof_during_read);
object * CVAR(uncaught_excptn);
object * CVAR(file_error);
object * CVAR(handler);
object * CVAR(abort_recur_err);
object * CVAR(def_handler);
object * CVAR(catch);
object * CVAR(throw);
object * CVAR(handler_exit);
object * CVAR(handler_abort);
