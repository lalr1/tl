#include "stream-lispsyms.h"
#include "stream-lispdecls.h"
#define ADDSUBR(name) CVAR(name)=addsubr(name ## _struct)

void stream_lispinit ()
{
# 1 "<stdin>"
# 1 "<built-in>"
# 1 "<command-line>"
# 1 "/usr/include/stdc-predef.h" 1 3 4
# 1 "<command-line>" 2
# 1 "<stdin>"
ADDSUBR(streamp);
ADDSUBR(read_strm_p);
ADDSUBR(write_strm_p);
ADDSUBR(get_curr_out);
ADDSUBR(set_output);
ADDSUBR(get_curr_in);
ADDSUBR(set_input);
ADDSUBR(write_char);
ADDSUBR(read_ch);
ADDSUBR(unread_char);
}
