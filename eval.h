struct estack_entry {
     char type;
     object *exp;
     object **bindings;
     object *protected;
     struct estack_entry *prev;
};

/* Functions used by other modules */
void unwind_eval_stack(struct estack_entry *);
void pop_eval_stack();
void evalinit();
void eval_lispinit();
void evalfinish();

/* Current top of eval-stack. */
struct estack_entry *estack_top;

#include "include/eval-lispsyms.h"
#include "include/eval-lispdecls.h"
