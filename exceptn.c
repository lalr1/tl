/* Exception handling.

 * There are two kinds of exception-handler. One is the `catch-point',
 * which lasts only as long as evaluation of the BODY argument to
 * catch(). A catch-point, when thrown to, exits non-locally to the
 * instance of catch() where it was set, and catch() then calls the
 * handler with the arguments passed to throw() (if the handler
 * doesn't satisfy `functionp', then it's evalled and returned
 * instead). The other kind is the `global handler', which, while not
 * necessarily permanent, persists as long as it is not
 * removed. Calling it does not cause a non-local exit (though it
 * should do that by itself). Global handlers are called when there
 * are no active catch-points for a symbol. The other difference
 * between catch-points and global handlers is that the latter are
 * called before the eval- and bind-stacks are unwound. The idea is
 * that an interpreter (say) could set a global handler for `error'
 * (covering all errors) which when called would print a backtrace
 * and/or enter a debugger, while catch-points would be useful for
 * non-error exceptions and signals (like EOF from read(), or POSIX
 * and BSD-style signals) and for general-purpose non-local exits
 * (though operating-system signals would be better dealt with using a
 * global handler).

 * A global handler can be any Lisp-level function. They are stored in
 * the `handler' property of a symbol, and are always called with two
 * arguments (see throw() for details). They must take care of
 * stack-unwinding themselves, and should not return. If they do, an
 * `uncaught-exception' error is thrown. If that itself is not caught,
 * we abort. */

#include "tl.h"
#include "exceptn.h"

#include "eval.h"
#include "mem.h"
#include "print.h"
#include "stream.h"
#include "subr.h"

void default_err();
void ob_fail();

int err_in_progress=0;
int backtrace_enab=1;

static jmp_buf *usr_abort;

/* Catch-point information. These are allocated in an obstack. */
struct catch_point {
     object *symbol;		/* Exception symbol. */
     object *handler;		/* Handler. */
     jmp_buf jump;		/* Where throw() should jump to. */
     object *handler_args;	/* Arguments to handler, set by throw(). */
     struct estack_entry *frame; /* Eval-stack frame of catchpoint. */
     struct catch_point *prev;	/* Previous catch-point, 0 if none. */
};


/* Last element in the chain of active catch-points. throw() chases
 * `prev' pointers to find the catch-point associated with the symbol
 * it was called with. This 0 marks the end of the list, in that
 * `cpstack_top' is assigned to the address of a new catch-point, whose
 * `prev' pointer is set to the previous value of `cpstack_top', making a
 * linked list within the stack.  */
static struct catch_point *cpstack_top=0;

DEFSYM("errors", CVAR(errors));
DEFSYM("error", CVAR(error));
DEFERRSYM("void-function", CVAR(void_func)); /* Error symbols */
DEFERRSYM("void-variable", CVAR(void_var));
DEFERRSYM("void-object", CVAR(void_obj));
DEFERRSYM("wrong-type", CVAR(wrong_type));
DEFERRSYM("set-constant-symbol", CVAR(const_sym));
DEFERRSYM("wrong-number-of-args", CVAR(wrong_num_args));
DEFERRSYM("invalid-function", CVAR(invalid_func));
DEFERRSYM("invalid-read-syntax", CVAR(invalid_syntax));
DEFERRSYM("index-out-of-bounds", CVAR(index_oob));
DEFERRSYM("eof-during-read", CVAR(eof_during_read));
DEFERRSYM("uncaught-exception", CVAR(uncaught_excptn));
DEFERRSYM("file-error", CVAR(file_error));

DEFSYM("handler", CVAR(handler));
DEFVAR("abort-on-recursive-error", CVAR(abort_recur_err), CVAR(t));

/* Default error handler. longjmp()s to the jmp_buf passed to
 * set_def_handler(). */
void default_err()
{
     unwind_eval_stack(NULL);
     cpstack_top=0;

     /* set_def_handler(), if called, will have filled `usr_abort'
      * with the address of a jmp_buf, typically representing the
      * top-level REPL. */
     if (usr_abort)
	  longjmp(*usr_abort, 1);
     else
	  abort();		/* Nothing to be done. */
}

DEFUN("default-handler", def_handler, 2, 2)
  (object *e, object *a)
{
     if (err_in_progress && !NULLP(SYMVAL(CVAR(abort_recur_err)))) {
	  fputs("error within error, aborting.\n", stderr);
	  abort();
     }
     err_in_progress=1;
     writs(" error: ", CVAR(nil));
     prin1(e, CVAR(nil));
     writs(": ", CVAR(nil));
     for ( ; !NULLP(a); a=cdr(a)) {
	  prin1(car(a), CVAR(nil));
	  if (!NULLP(cdr(a)))
	       writs(", ", CVAR(nil));
	  else
	       writc('\n', CVAR(nil));
     }
     if (backtrace_enab) {
	  writs(" backtrace:\n", CVAR(nil));
	  pop_eval_stack();	/* Get rid of error-handler entry. */
	  btrace(CVAR(nil));
     }
     err_in_progress=0;
     default_err();
     return 0;
}


void set_def_handler(jmp_buf *j)
{
     usr_abort=j;
     put(CVAR(error), CVAR(handler), CVAR(def_handler));
}

void ob_fail()
{
     fputs("obstack alloc failed.\n", stderr);
     exit(1);
}

/* Establish a return point for a later non-local exit. This can be
 * used for normal exceptions (i.e. those that aren't errors) or for
 * actual errors --- the difference being only that error-symbols are
 * considered so by virtue of their `errors' property (like in Emacs
 * Lisp), so that one can catch any error with:
    (catch 'error ( ... ) ... )
 * assuming that all error-symbols have at least `error' in their
 * `errors' property. */
DEFUN("catch", catch, 2, UNEVALLED(3))
  (object *sym, object *body, object *handlr)
{
     struct catch_point c;

     c.symbol=sym=eval(sym);
     struct estack_entry *top=estack_top;

     c.frame=top;
     c.handler=handlr;
     c.prev=cpstack_top;
     cpstack_top=&c;
     c.handler_args=CVAR(nil);
     object *ret;
     if (!setjmp(c.jump)) {
	  /* Evaluate the body. If there is a throw() to SYM, then
	   * we'll end up in the following `else'-clause.  */
	  ret=eval(body);
	  /* This won't be reached if there is a throw(). */
	  unwind_eval_stack(top);
     } else {
	  /* We've caught our exception. Undo all local bindings made
	   * since the catch-point was established, then call the
	   * handler with the arguments passed to throw(), minus the
	   * exception-symbol itself. */
	  sym=c.symbol;
	  handlr=c.handler;
	  top=c.frame;
	  /* Unwind back to our frame. */
	  unwind_eval_stack(top);
	  /* Call the handler, if it looks like a function; eval and
	   * return it otherwise, unless it's `nil', in which case
	   * simply return the arguments passed to `throw'. */
	  if (!NULLP(functionp(handlr)))
	       ret=apply(2, make_list(2, handlr, c.handler_args));
	  else if (!NULLP(handlr))
	       ret=eval(handlr);
	  else
	       ret=c.handler_args;
     }
     
     cpstack_top=c.prev;

     /* The return value is that of the body if all was well, or that
      * of the handler otherwise. */
     return ret;
}

/* Throw an exception. If the thrown symbol has an `errors' property,
 * then it's considered an error, and we search for a handler for any
 * of the symbols in that property. If it has none, or if `errors' is
 * `nil', consider it a normal (non-critical) exception.  If there is
 * an appropriate catch-point we longjmp() to the corresponding
 * catch(). The catch-point handler is called with all of the
 * arguments passed to throw(), minus the exception-symbol. Global
 * handlers are passed two arguments; the error-symbol and the rest of
 * the arguments to throw().  */
DEFUN("throw", throw, 1, ARGS_UNLIMITED)
  (int c, object *v)
{
     object *sym=car(v);
     struct catch_point *e=cpstack_top;
     /* Find the innermost catch-point associated with SYM, or with
      * one of the symbols from its `errors' property, if it has
      * one. */
     object *o=get(sym, CVAR(errors));
     for ( ;
	   e && ( (!NULLP(o) && NULLP(memq(e->symbol, o)))
		     || e->symbol != sym );
	   e=e->prev)
	  ;
     if (!e) {			/* There was none. */
	  /* Try to find a global handler for the symbol or one of its
	   * associated errors. */
	  object *p=get(sym, CVAR(handler)), *q=o;
	  if (NULLP(p) && !NULLP(o)) /* No handler for SYM. */
	       for ( ;		/* cdr down `errors'. */
		    NULLP(p) && !NULLP(q);
		     p=get(car(q), CVAR(handler)), q=cdr(q))
		    ;
	  if (!NULLP(p))
	       funcall(3, cons(p, make_list(2, car(v), cdr(v))));
	  /* If we have already thrown `uncaught-exception', abort
	   * (and dump core). This should be cleaner. Perhaps exit()
	   * would be better. Meh. */
	  if (sym==CVAR(uncaught_excptn))
	       abort();
	  else
	       /* There may be a handler for this, so we might as well
		* throw it. */
	       throw(2, make_list(2, CVAR(uncaught_excptn), sym));
     } else {
	  /* Pass the arguments we received to the handler, via
	   * catch(). */
	  e->handler_args=cdr(v);
	  longjmp(e->jump, 1);
     }
     return 0;			/* Never reached (we hope). */
}

DEFUN("handler-exit", handler_exit, 0, ARGS_UNLIMITED)
  (int c, object *v)
{
     exit(1);
}

DEFUN("handler-abort", handler_abort, 0, ARGS_UNLIMITED)
  (int c, object *v)
{
     abort();
}

#include "include/exceptn-lispinit.h"

void siginit() 
{
     obstack_alloc_failed_handler=ob_fail;
     exceptn_lispinit();
}

void sigfinish()
{
  return;
}
