#define ARR_TYPE_MASK 1<<16
#define STRING 0		/* string -> bit 16 clear */
#define VECTOR 1<<16		/* vector -> bit 16 set */
#define ARR_PTR(o) ((struct array *)(o)->ptr.array)
#define ARRAYP(o) (TYPE(o)==ARRAY)
#define SET_STRING(o) (o)->type&=~(ARR_TYPE_MASK)
#define SET_VECTOR(o) (o)->type|=ARR_TYPE_MASK
#define SET_ARRLEN(o, ln) ARR_PTR(o)->length=(ln)
#define STRINGP(o) (ARRAYP(o) && (((o)->type & (ARR_TYPE_MASK)) == STRING))
#define STRREF(o) (ARR_PTR(o)->type.string)
#define VECTORP(o) (ARRAYP(o) && (((o)->type & (ARR_TYPE_MASK)) == VECTOR))
#define VECREF(o) (ARR_PTR(o)->type.vector)
#define ARRAYLEN(o) (ARR_PTR(o)->length)

struct type_spec {
     char *name;
     object * (*alloc)(void);
     void (*mark)(object *);
     void (*print)(object *, object *);
     object * (*compare)(object *, object *);
     void (*free)(object *);
};

struct type_spec **types;
int type_max;

object *make_string(char *);
object *array_to_vector(int, object **);
object *make_vector(int, object *);

void typeinit();
void typefinish();

int register_type(struct type_spec *);

#include "include/types-lispsyms.h"
#include "include/types-lispdecls.h"
