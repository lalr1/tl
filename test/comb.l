;; Evaluate expressions of combinatory logic

(fset '$ (symbol-function 'funcall))
(defun comb (e)
  (cond ((atom e) (cond ((eq e 's) '(lambda (f) 
				     (lambda (g)
				       (lambda (x)
					 ($ ($ f x) ($ g x))))))

			((eq e 'k) '(lambda (x)
				     (lambda (y) x)))

			((eq e 'i) '(lambda (x) x))

			(t (symbol-value e))))

	(t ($ (comb (car e)) (comb (car (cdr e)))))))

(comb '(((s k) k) i))			;should return `(lambda (x) x)' [i.e. `i']

;(gc)
;; let `$' = `funcall'
;; Y =
;; (lambda (f) '($ (lambda (x) ($ f ($ x x))) (lambda (x) ($ f ($ x x)))))
;; =
;; ^f.( ^x.(f (x x)) ^x.(f (x x)) )
;; = 
;; ^f.( ((s (k f)) ((s i) i)) ((s (k f)) ((s i) i)) )
;; =
;; ``s ``s``s`ks``s`kki``s``s`ks`ki`ki ``s``s`ks``s`kki``s`ks`ki`ki
;; =
;; ((s
;;   ((s ((s (k s)) ((s (k k)) i))) ((s ((s (k s)) (k i))) (k i))))
;;  ((s ((s (k s)) ((s (k k)) i))) ((s ((s (k s)) (k i))) (k i))))

;;      v = ^x.v
;; let  a = ^q.^x:q
;; ->  (a v) = v
;; so v is a fixed point of a
;; so v = (Y a)
;; but a == k (by alpha-conversion)
;; so v = (Y k)

;(comb '(
;	((s
;	  ((s ((s (k s)) ((s (k k)) i))) ((s ((s (k s)) (k i))) (k i))))
;	 ((s ((s (k s)) ((s (k k)) i))) ((s ((s (k s)) (k i))) (k i))))
;	k))
