;; Peano arithmetic in Lisp
;(set 'max-objects 2048)
(defun pzerop (n)
  (null n))

(defun S (n)
  (cons t n))

(defun P (n)
 (cdr n))

(defun make-number (n)
  (cond ((eq n 0) nil)
	(t (S (make-number (- n 1))))))

(defun get-number (n)
  (cond ((pzerop n) 0)
	(t (+ 1 (get-number (P n))))))

(set 'zero nil)
(set 'one (S zero))
(set 'two (S one))
(set 'three (S two))

(defun pequal (x y)
  (cond ((pzerop x) (pzerop y))
	(t (pequal (P x) (P y)))))

(defun add (x y)
  (cond ((pzerop y) x)
	(t (add (S x) (P y)))))

(defun sub (x y)
  (cond ((pzerop x) zero)
	((pzerop y) x)
	(t (sub (P x) (P y)))))

(pequal three (add one two))		; 1 + 2 = 3
(pequal one (sub three two))		; 3 - 2 = 1

(defun mul (x y)
  (cond ((pzerop x) zero)
	((pzerop y) zero)
	((pzerop (P y)) x)
	(t (add x (mul x (P y))))))

(set 'five (make-number 5))
(set 'seven (make-number 7))
(set 'twenty (make-number 20))
(get-number (add five seven))

(get-number (mul five seven))		; This uses about 9000 objects.
(get-number (mul five twenty))
(heap-info)

