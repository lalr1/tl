;(set-macro-char ?' 
;		(lambda (c s) 
;		  (list (quote quote) (read s))))

(defun mapcar (func list)
  (cond ((null list) nil)
	(t (cons (funcall func (car list))
		 (mapcar func (cdr list))))))

(defmacro setq (var val)
  (list 'set (list 'quote var) val))

(setq a :b) (setq b :c)

(mapcar 'symbol-value '(a b))		;should return (:b :c)

(setq a '((b . cat) (a . camel) (z . cow)))

(defun assoc (key list)
 (cond ((eq key (car (car list))) (cdr (car list)))
       (t (assoc key (cdr list)))))

(assoc 'z a)				;should return `cow'
(assoc 'b a)
(heap-info)
