(defun mapcar (func list)
 (cond ((null list) nil)
       (t (cons (funcall func (car list)) (mapcar func (cdr list))))))

(defmacro setq (var val)
 (list 'set (list 'quote var) val))

(setq a 'cat)
(setq b 'zebra)
(setq c 'giraffe)

(defun equal (x y)
 (cond ((atom x) (cond ((atom y) (eq x y)) (t nil)))
       (t (cond ((equal (car x) (car y)) (equal (cdr x) (cdr y)))))))

(eq '(a b) '(a b))			;nil
(equal '(a b) '(a b))			;t
(setq e (list 'a 'b 'c))
(setq d (list a b c))
;(gc)
(equal d (mapcar 'symbol-value e))	;t
(heap-info)

