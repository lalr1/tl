(defun macroexpand (form) (apply (cdr (symbol-function (car form))) 
				 (cdr form)))
(defun mapcar (f l)
 (cond ((null l) nil)
       (t (cons (funcall f (car l)) (mapcar f (cdr l))))))

(defun cadr (x) (car (cdr x)))

(defmacro let (list &rest body)
 ((lambda (ls) (cons (cons 'lambda (cons (car ls) body)) (cdr ls)))
   ((lambda (l) (cons (mapcar 'car l) (mapcar 'cadr l))) list)))

(set 'form '(let ((x 8) (y 13)) (print y) x))
(macroexpand form)
(eval form)

